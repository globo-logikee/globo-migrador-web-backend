.PHONY: test_prod test_qa deploy_prod deploy_qa

## lists all available targets
list:
	@echo "Please, select one:"
	@sh -c "$(MAKE) -p no_targets__ | awk -F':' '/^[a-zA-Z0-9][^\$$#\/\\t=]*:([^=]|$$)/ {split(\$$1,A,/ /);for(i in A)print A[i]}' | grep -v '__\$$' | grep -v 'make\[1\]' | grep -v 'Makefile' | sort"
# required for list
no_targets__:


## QA

test_qa:
	@echo "Not done yet"

info_qa:
	tsuru app info --app migradorweb-api-dev | cat

deploy_qa:
	tsuru app deploy --app migradorweb-api-dev .
	@$(MAKE) info_qa
	@$(MAKE) test_qa

## PROD

test_prod:
	@echo "Not done yet"

info_prod:
	tsuru app info --app migradorweb-api-prod | cat

deploy_prod:
	tsuru app deploy --app migradorweb-api-prod .
	@$(MAKE) info_prod
	@$(MAKE) test_prod

# EOF
