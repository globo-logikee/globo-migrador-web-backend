module.exports = {
    "name": process.env.CONNECTION_NAME,
    "type": "mongodb",
    "host": process.env.DBAAS_MONGODB_HOSTS,
    "port": process.env.DBAAS_MONGODB_PORT,
    "username": process.env.DBAAS_MONGODB_USER,
    "password": process.env.DBAAS_MONGODB_PASSWORD,
    "database": process.env.DBAAS_MONGODB_DB_DATABASE,
    "useNewUrlParser": true,
    "useUnifiedTopology": true,
    "synchronize": true,
    "entities": [
        process.env.NODE_ENV != 'PRODUCTION' ?
            "src/entities/**/*.ts" :
            "dist/entities/*.js"
    ]
}