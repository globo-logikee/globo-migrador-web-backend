import { NextFunction, Request, Response } from "express";
import { verify } from "jsonwebtoken";

import { UserRepository } from '../../repositories/implementation';

const userRepo = new UserRepository();

export default async (req: Request, res: Response, next: NextFunction) => {
    const header = req.header('Authorization');
    if (!header) {
        return res.status(401).json({ message: 'authorization_token_required' });
    }

    const parts = header.split(' ');
    if (parts.length === 1) {
        return res.status(401).json({ message: 'token_badly_formated' });
    }

    const [scheme, token] = parts;
    if (!/^Bearer$/i.test(scheme)) {
        return res.status(401).json({ message: 'not_bearer_type' });
    }

    verify(token, process.env.TOKEN_SECRET, async (err, decoded) => {
        if (err) return res.status(401).json({ message: 'err_verify_token' });

        const { globoId, username } = decoded;
        const user = await userRepo.finbByGloboId(globoId);
        if (!user) {
            return res.status(401).json({ message: 'user_not_found' });
        }

        if (user.username != username) {
            return res.status(401).json({ message: 'unauthorized' });
        }

        next();
    });
}