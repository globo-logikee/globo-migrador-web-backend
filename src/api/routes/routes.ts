import express from 'express';

import auth from '../middlewares/Auth';

import {
    indexController,
    chooserController,
    saveNewAccountController,
    refuseNewAccountController,
    regretController,
    firstCommunicationController,
    getUsersSalesForceController,
    regretUserSalesForceController,
    reactivateUsersSalesForceController,
    generateTokenController,
    userAccessedController,
    removeServiceSalesForceController
} from '../../useCases';

import { compareController } from '../../useCases/createMockedCompare';

class Routes {
    public router: express.Router;

    constructor() {
        this.router = express.Router();
        this.salesforceRoutes();
        this.apiRoutes();
    }

    // Rotas para consumo interno da api
    apiRoutes(): void {
        this.router.post('/hash', async (req, res) => await generateTokenController.handle(req, res));
        // Rota temporario para criação de mock da tabela de d/para
        this.router.post('/compare', async (req, res) => await compareController.handle(req, res));

        this.router.use(auth);
        this.router.post('/index', async (req, res) => await indexController.handle(req, res));
        this.router.post('/chooser/:accountId/:globoId', async (req, res) => await chooserController.handle(req, res));
        this.router.post('/save/account', async (req, res) => await saveNewAccountController.handle(req, res));
        this.router.post('/refuse/account', async (req, res) => await refuseNewAccountController.handle(req, res));
        this.router.post('/regret', async (req, res) => await regretController.handle(req, res));
    }

    // Rotas para consumo da salesforce
    salesforceRoutes(): void {
        this.router.post('/v1/users/services/remove', async (req, res) => await removeServiceSalesForceController.handle(req, res));
        this.router.get('/v1/users/accessed', async (req, res) => await userAccessedController.handle(req, res));
        this.router.post('/v1/users', async (req, res) => await firstCommunicationController.handle(req, res));
        this.router.put('/v1/users', async (req, res) => await regretUserSalesForceController.handle(req, res));
        this.router.put('/v1/users/reactivate', async (req, res) => await reactivateUsersSalesForceController.handle(req, res));
        this.router.post('/v1/salesforce/users', async (req, res) => await getUsersSalesForceController.handle(req, res));
    }
}

export default new Routes().router;