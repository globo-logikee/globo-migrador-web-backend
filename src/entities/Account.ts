import {
    Column,
    CreateDateColumn,
    Entity,
    JoinColumn,
    ManyToOne,
    ObjectIdColumn,
    UpdateDateColumn
} from "typeorm";
import { User } from "./User";

export enum PROCCESS_STATUSES {
    SALESFORCE_INPUT = 'SALESFORCE_INPUT',
    CREATED = 'CREATED',
    WAITING_USER_CONFIRM = 'WAITING_USER_CONFIRM',
    WAITING_OPT_IN = 'WAITING_OPT_IN',
    WAITING_OPT_OUT = 'WAITING_OPT_OUT',
    REGRET_OPT_IN = 'REGRET_OPT_IN',
    REGRET_OPT_OUT = 'REGRET_OPT_OUT',
    WAITING_ACCEPT_LW_IN = 'WAITING_ACCEPT_LW_IN',
    WAITING_ACCEPT_LW_OUT = 'WAITING_ACCEPT_LW_OUT',
    OPT_IN_DONE = 'OPT_IN_DONE',
    OPT_OUT_DONE = 'OPT_OUT_DONE',
    NOT_ACCEPT_OPT_OUT_DONE = 'NOT_ACCEPT_OPT_OUT_DONE',
    // errors statuses
    BAD_REQUEST_LW = 'BAD_REQUEST_LW',
    UNPROCESSABLE_ENTITY_LW = 'UNPROCESSABLE_ENTITY_LW',
    ALREADY_EXISTS_LW = 'ALREADY_EXISTS_LW',
    MAILBOX_NOT_FOUND_LW = 'MAILBOX_NOT_FOUND_LW',
    DOMAIN_NOT_FOUND_LW = 'DOMAIN_NOT_FOUND_LW',
    REACTIVATED = 'REACTIVATED' 
}

export interface phones {
    cellPhone?: string;
    dddCellPhone?: string;
    phone?: string;
    dddPhone?: string;
}

interface address {
    city: { id: number, ibgeId: number, name: string };
    state: { id: number, name: string, abbreviation: string, ibgeId: number };
    country: { id: number, name: string };
    neighborhood: string;
    zipCode: string;
    address1: string;
    addressType: string;
    number: number;
}

@Entity('accounts')
export class Account {
    @ObjectIdColumn({ name: '_id' })
    readonly id: string;

    @Column(
        { 
            default: PROCCESS_STATUSES.CREATED,
            type: 'enum'
        }
    )
    processStatus: PROCCESS_STATUSES;

    @Column()
    globoId: string;

    @Column()
    bannerSended?: boolean;

    @Column()
    userIp?: string;

    @Column({ default: 3 })
    status_opt: number;

    @Column()
    decisionDate?: string;

    @Column()
    fullName: string;

    @Column()
    globoMail: string;

    @Column()
    docNumber: string;

    @Column()
    firstCommunicationDate?: string;

    @Column()
    effectiveActionDate?: string;

    @Column()
    globoIdDeactivationDate?: string;

    @Column({ default: null })
    action?: number;

    @Column()
    username: string;

    @Column()
    serviceAlreadyExists: boolean;

    @Column()
    serviceId: number;

    @Column({ unique: true })
    email: string;

    @Column()
    docType: string;

    @Column({ default: [] })
    phones?: phones;

    @Column()
    contactEmail: string;

    @Column()
    alternativeEmail?: string;

    @Column({ default: false })
    reactivated?: boolean;

    @Column()
    newEmail?: string;

    @Column()
    length?: number;

    @Column()
    password?: string;

    @Column()
    opt?: 'in' | 'out';

    @Column({ type: 'timestamp' })
    migrationDate?: Date;

    @Column()
    reactivatedUntil?: string;

    @Column({ type: 'timestamp' })
    reactivatedUntilDate?: Date;

    @Column()
    changedDecision?: any;

    @Column({ default: false })
    haveServiceEightyFour?: boolean;

    @Column()
    lote?: string;

    @Column()
    changedDecisionDate?: string;

    @Column()
    scheduledTo?: string;

    @Column()
    address: address;

    @ManyToOne(type => User, user => user.accounts)
    user: User;

    @Column()
    userId: string;

    @CreateDateColumn({ type: 'timestamp' })
    createdAt?: string;

    @UpdateDateColumn({ type: 'timestamp' })
    updatedAt?: string;

    constructor(props: Omit<Account, 'id'>) {
        Object.assign(this, props);
    }
}