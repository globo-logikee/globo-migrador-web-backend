import { Column, Entity, ObjectIdColumn } from "typeorm";

@Entity('CommunicationSalesforce')
export class CommunicationSalesforce {
    @ObjectIdColumn({ name: '_id' })
    readonly id: string;

    @Column()
    firstCommunicationDate: string;

    @Column()
    globoId: string;

    @Column()
    serviceId: string;

    constructor(props: Omit<CommunicationSalesforce, 'id'>) {
        Object.assign(this, props);
    }
}