import { Column, CreateDateColumn, Entity, ObjectIdColumn, UpdateDateColumn } from "typeorm";

@Entity('compare')
export class Compare {
    @ObjectIdColumn({ name: '_id' })
    readonly id: string;

    @Column()
    globoId: string;

    @Column()
    globoMailFree: string;

    @Column()
    lwmail: string;

    @CreateDateColumn({ type: 'timestamp' })
    createdAt?: string;

    @UpdateDateColumn({ type: 'timestamp' })
    updatedAt?: string;

    constructor(props: Omit<Compare, 'id'>) {
        Object.assign(this, props);
    }
}