import {
    Column,
    CreateDateColumn,
    Entity,
    ObjectIdColumn,
    UpdateDateColumn
} from "typeorm";

export const PROCCESS_MESSAGES = {
    USER_LOGIN: (user: string): string => `Login do usuário: ${user}`,
    USER_LOGOUT: (user: string) => `Logout do usuário: ${user}`,
    EXPIRED: 'Fora do prazo',
    SERVICE_UNAUTHORIZED: (serviceId: number) => `Serviço ${serviceId} não autorizado globoId`,
    SELECTED_ACCOUNT: (account: string) => `Conta selecionada: ${account}`,
    OPT_IN: 'Opt-in',
    OPT_OUT: 'Opt-out',
    NEW_ACCOUNT_LOCALWEB: (account: string) => `Nova conta localweb gerada: ${account}`,
    NEW_PASSWORD: 'Nova senha gerada',
    MIGRATION_DATE: (date: string) => `Data da migração gerada: ${date}`,
    REGRET: (opt: 'in' | 'out') => `Arrependimento opt-${opt}`,
    SERVICE_REMOVED: (globoId: string, serviceId: number) => `Usuário com o globoId ${globoId} teve o serviço ${serviceId} removido.`,
    // Error messages
    ERR_REMOVE_SERVICE_SALESFORCE_NOT_FOUND: (globoId: string) => `Err 404 delete service salesforce - globoId ${globoId} not found`,
    ERR_REMOVE_SERVICE_SALESFORCE_INTERNAL_SERVER: (globoId: string) => `Err 500 delete service salesforce - globoId ${globoId}`,
    ERR_GLOBO_ID_API_DISABLE_IN: (globoId: string, message?: string) => `err 400 ${message} worker globo id api disable in`,
    ERR_GET_LOCAWEB: (err: string, globoId: string) => `${err} get locaweb globoId: ${globoId}`,
    ERR_CREATE_LOCAWEB: (err: string, email: string, newEmail: string) => `${err} locaweb: ${email} para ${newEmail}`,
    ERR_UPDATE_LOCAWEB: (err: string, fieldName?: string, field?: string) => `${err} locaweb: ${fieldName} ${field}`,
    ERR_GLOBOID_API: (err: string) => `${err} globoIdApi`
}

@Entity('logs')
export class Log {
    @ObjectIdColumn({ name: '_id' })
    readonly id: string;

    @Column()
    globoId: string;

    @Column()
    serviceId?: number;

    @Column()
    username: string;

    @Column()
    userIp?: string;

    @Column()
    message: string;

    @Column({ nullable: true })
    apiMessage?: string;

    @CreateDateColumn({ type: 'timestamp' })
    createdAt?: string;

    @UpdateDateColumn({ type: 'timestamp' })
    updatedAt?: string;

    constructor(props: Omit<Log, 'id'>) {
        Object.assign(this, props);
    }
}
