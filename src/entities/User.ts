import {
    Column,
    CreateDateColumn,
    Entity,
    ObjectIdColumn,
    OneToMany,
    UpdateDateColumn
} from "typeorm";
import { Account } from "./Account";

@Entity('users')
export class User {
    @ObjectIdColumn({ name: '_id' })
    readonly id: string;

    @Column({ default: false })
    accessed?: boolean;

    @Column({ unique: true })
    globoId: string;

    @Column()
    fullName: string;

    @Column({ unique: true })
    contactEmail: string;

    @Column()
    alternativeEmail?: string;

    @Column()
    serviceIds: { firstCommunication: string, serviceId: number }[];

    @Column()
    username: string;

    @OneToMany(type => Account, account => account.user)
    accounts?: Account[];

    @CreateDateColumn({ type: 'timestamp' })
    createdAt?: string;

    @UpdateDateColumn({ type: 'timestamp' })
    updatedAt?: string;

    constructor(props: Omit<User, 'id'>) {
        Object.assign(this, props);
    }
}