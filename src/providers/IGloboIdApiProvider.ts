import { SmartInterventionKeys, SmartInterventionValues } from "./implementation";

export interface IGloboIdApiProvider {
    getUserData(globoId: string): Promise<any>;
    getUserServices(globoId: string): Promise<any[]>;
    getServiceAuthorize(globoid: string, serviceId: number, userIp?: string): Promise<any>;
    removeService(globoId: string, serviceId: number): Promise<any>;
    insertService(globoId: string, serviceId: number): Promise<any>;
    updateToLwMail(globoId: string, lwMail: string): Promise<any>;
    removeSmartIntervention(key: SmartInterventionKeys, globoid: string): Promise<any>;
    insertSmartIntervention(key: SmartInterventionKeys, value: SmartInterventionValues, globoid: string): Promise<any>
}