export interface IHasherProvider {
    encrypt(string: string): Promise<string>;
    compare(string: string, encrypted: string): Promise<boolean>;
    decrypt(string: string): Promise<string>
}