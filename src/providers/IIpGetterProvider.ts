export interface IIpGetterProvider {
    getMyIp(): Promise<{ data: { ip: string } }>;
}