import { Account } from "../entities";
import { actionEnum } from "../utils/createBannerPayloLocaweb";
import { ILocaWebCreateModel } from "../utils/ILocaWeb";

export interface ILocaWebProvider {
    createLocaweb(accounts: ILocaWebCreateModel[]): Promise<any>;
    consultStatus(ids_globo: { id_globo: string }[]): Promise<any>;
    updateLocaweb(isPro: boolean, account: string, enabled: boolean): Promise<any>;
    sendBannerLocaweb(account: Account, action: actionEnum): Promise<any>;
}