import { IUsersSalesForceResponseDTO } from "../utils/parseAccountToSalesforce";

export interface ISalvesForceProvider {
    sendToSalesForce(salesforceUser: IUsersSalesForceResponseDTO): Promise<any>;
}