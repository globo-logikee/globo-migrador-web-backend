export interface ITokenProvider {
    generate(params: {}): Promise<string>;
}