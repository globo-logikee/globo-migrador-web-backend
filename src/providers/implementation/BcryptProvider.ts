import { IHasherProvider } from "../IHasherProvider";
import { 
    createCipheriv, 
    createDecipheriv, 
    createHash, 
    randomBytes,
    createCipher,
    createDecipher
} from 'crypto';

export class BcryptProvider implements IHasherProvider {
    public secret: string;
    public key: any
    public iv: any; 
    public alg: string;

    constructor() {
        this.secret = process.env.PASSWORD_SECRET;
        this.alg = process.env.PASSWORD_ALG;
        this.iv = process.env.PASSWORD_IV;
        this.key = createHash('sha256').update(String(this.secret)).digest('base64').substr(0, 32);
    }

    async encrypt(string: string): Promise<string> {
        const cipher = createCipheriv(this.alg, this.key, this.iv);
        const ecrypted = cipher.update(string, 'utf-8', 'hex');
        return ecrypted;

    }
    async compare(string: string, encrypted: string): Promise<boolean> {
        // return bcrypt.compareSync(string, encrypted);
        return true;
    }
    async decrypt(string: string): Promise<string> {
        // const decipher = createDecipher(this.alg, this.key);
        const decipher = createDecipheriv(this.alg, this.key, this.iv);
        const decrypted = decipher.update(string, 'hex', 'utf8');

        return decrypted;
    }
}