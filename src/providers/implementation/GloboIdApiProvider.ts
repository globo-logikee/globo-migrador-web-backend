import { IGloboIdApiProvider } from "../IGloboIdApiProvider";
import axios from 'axios';
import { URLSearchParams } from "url";

export enum SmartInterventionKeys {
    smart_intervention_globomail_free = 'smart_intervention_globomail_free',
    smart_intervention_globomail_pro = 'smart_intervention_globomail_pro'
}

export enum SmartInterventionValues {
    descontinuacao_globomail_free = 'descontinuacao_globomail_free',
    descontinuacao_globomail_pro = 'descontinuacao_globomail_pro'
}

export class GloboIdApiProvider implements IGloboIdApiProvider {
    private basicToken: string;
    private urlGetToken: string;
    private urlGloboIdApi: string;
    private requestContext: string
    private myIp: string;
    private smartInterventionUrl: string;

    constructor() {
        this.basicToken = process.env.CRENDETIAL_TO_GET_TOKEN;
        this.urlGetToken = process.env.URL_TO_GET_TOKEN;
        this.urlGloboIdApi = process.env.URL_GLOBO_ID_API;
        this.requestContext = process.env.X_REQUEST_CONTEXT;
        this.myIp = process.env.MY_IP;
        this.smartInterventionUrl = process.env.SMART_INTERVENTION_URL;
    }

    private async getApiHeaders(): Promise<{ Authorization: string; "X-Request-Context": string }> {
        const params = new URLSearchParams();
        params.append('grant_type', 'client_credentials');

        const config = {
            headers: {
                'Authorization': `Basic ${this.basicToken}`,
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        };

        const accessToken = await axios.post(
            `${this.urlGetToken}`,
            params,
            config
        );

        return {
            'Authorization': `Bearer ${accessToken.data.access_token}`,
            'X-Request-Context': this.requestContext
        };
    }

    async getUserData(globoId: string): Promise<any> {
        try {
            const config = await this.getApiHeaders();
            const globoUser = await axios.get(`${this.urlGloboIdApi}/users/${globoId}`, { headers: config });

            return globoUser.data;
        } catch (error) {
            // TODO
            throw error;
        }
    }

    async getUserServices(globoId: string): Promise<any[]> {
        try {
            const config = await this.getApiHeaders();
            const userServices = await axios.get(`${this.urlGloboIdApi}/users/${globoId}/services`, { headers: config });

            return userServices.data;
        } catch (error) {
            // TODO
            throw error;
        }
    }

    async getServiceAuthorize(globoid: string, serviceId: number, userIp?: string): Promise<any> {
        try {
            const config = await this.getApiHeaders();
            const body = {
                id: globoid,
                serviceId,
                ip: userIp ? userIp : this.myIp
            };
            const authorization = await axios.put(
                `${this.urlGloboIdApi}/authorize/globoid`,
                body,
                { headers: config }
            );

            return authorization.data;
        } catch (error) {
            // TODO
            throw error;
        }
    }

    async removeService(globoId: string, serviceId: number): Promise<any> {
        try {
            const config = await this.getApiHeaders();
            const body = {}
            const remove = await axios.delete(
                `${this.urlGloboIdApi}/users/${globoId}/services/${serviceId}`,
                { headers: config }
            );

            return remove;
        } catch (error) {
            throw error;
        }
    }

    async insertService(globoId: string, serviceId: number): Promise<any> {
        try {
            const config = await this.getApiHeaders();
            const body = {}
            const insert = await axios.put(
                `${this.urlGloboIdApi}/users/${globoId}/services/${serviceId}`,
                body,
                { headers: config }
            );

            return insert;
        } catch (error) {
            throw error;
        }
    }

    async updateToLwMail(globoId: string, lwMail: string): Promise<any> {
        try {
            const config = await this.getApiHeaders();
            const body = {
                ip: this.myIp,
                email: lwMail
            }
            const update = await axios.put(
                `${this.urlGloboIdApi}/users/${globoId}`,
                body,
                { headers: config }
            );

            return update.data;
        } catch (error) {
            throw error;
        }
    }

    async insertSmartIntervention(key: SmartInterventionKeys, value: SmartInterventionValues, globoid: string): Promise<any> {
        try {
            const config = await this.getApiHeaders();
            const body = {
                globoid,
                key,
                value
            };

            const insert = await axios.put(
                `${this.smartInterventionUrl}`,
                body,
                { headers: config }
            );

            return insert;
        } catch (error) {
            console.log('insert smart err', error.message);
            throw error;
        }
    }

    async removeSmartIntervention(key: SmartInterventionKeys, globoid: string): Promise<any> {
        try {
            const config = await this.getApiHeaders();
            const body = {
                globoid,
                key
            }

            const remove = await axios.delete(
                `${this.smartInterventionUrl}`,
                {
                    headers: config,
                    data: body
                }
            );

            return remove;
        } catch (error) {
            console.log('smart remove err', error.message);
            throw error;
        }
    }
}