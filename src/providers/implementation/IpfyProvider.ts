import { IIpGetterProvider } from "../IIpGetterProvider";

import axios from 'axios';

export class IpfyProvider implements IIpGetterProvider {
    private url: string;

    constructor() {
        this.url = process.env.URL_IP_FY;
    }

    async getMyIp(): Promise<{ data: { ip: string } }> {
        try {
            return await axios.get(`${this.url}/?format=json`);
        } catch (error) {
            throw error;
        }
    }

}