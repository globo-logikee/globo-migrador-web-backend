import { sign } from "jsonwebtoken";
import { ITokenProvider } from "../ITokenProvider";

export class JwtProvider implements ITokenProvider {
    private secret: string;

    constructor() {
        this.secret = process.env.TOKEN_SECRET;
    }

    async generate(params: {}): Promise<string> {
        return sign(params, this.secret, {
            expiresIn: 86400
        });
    }
}