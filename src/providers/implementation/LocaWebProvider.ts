import axios from "axios";
import HttpsProxyAgent from 'https-proxy-agent';
import { AccountRepository, UserRepository } from "../../repositories/implementation";
import { IUserRepository } from "../../repositories/IUserRepository";
import { Account } from "../../entities";
import { actionEnum, CreateBannerPayloadLocaweb } from "../../utils/createBannerPayloLocaweb";
import { ILocaWebCreateModel } from "../../utils/ILocaWeb";
import { ILocaWebProvider } from "../ILocaWebProvider";
import { IAccountRepository } from "../../repositories/IAccountRepository";
import format from "date-fns/format";

export class LocaWebProvider implements ILocaWebProvider {
    private urlLocaWeb: string;
    private urlLocaWebDomains: string;
    private globoDomainPro: string;
    private globoDomainFree: string;
    private basicToken: string;
    private accountRepository: IAccountRepository;
    httpsAgent: any;
    axiosInstance: any;

    constructor() {
        this.setAxiosProxyAgent();
        this.accountRepository = new AccountRepository();
        this.urlLocaWeb = process.env.URL_LOCAWEB;
        this.urlLocaWebDomains = process.env.URL_LOCAWEB_DOMAINS;
        this.globoDomainPro = process.env.GLOBO_DOMAIN_PRO;
        this.globoDomainFree = process.env.GLOBO_DOMAIN_FREE;
        this.basicToken = process.env.BASIC_TOKEN;
    }

    async getApiHeaders() {
        return {
            "Content-Type": "application/json",
            "Authorization": `Basic ${this.basicToken}`
        }
    }

    setAxiosProxyAgent() {
        if (process.env.PROXY) {
            const [host, port] = process.env.PROXY.split(':');
            this.httpsAgent = HttpsProxyAgent({ host, port });
            this.axiosInstance = axios.create({ httpsAgent: this.httpsAgent })
        } else {
            this.axiosInstance = axios;
        }
    }

    async consultStatus(ids_globo: { id_globo: string }[]): Promise<any> {
        try {
            const response = await this.axiosInstance.post(`${this.urlLocaWeb}/migration_status`, ids_globo);

            return response.data
        } catch (error) {
            throw error;
        }
    }

    async createLocaweb(accounts: ILocaWebCreateModel[]): Promise<any> {
        try {
            const response = await this.axiosInstance.post(`${this.urlLocaWeb}/migration`, accounts);

            return response;
        } catch (error) {
            throw error;
        }
    }

    async updateLocaweb(isPro: boolean, account: string, enabled: boolean): Promise<any> {
        try {
            const body = {
                enabled
            }

            const response = await this.axiosInstance.put(
                `${this.urlLocaWebDomains}/v2/domains/${isPro ? this.globoDomainPro : this.globoDomainFree}/mailboxes/${account}`,
                body,
                { headers: await this.getApiHeaders() }
            );

            return response.data;
        } catch (error) {
            throw error;
        }
    }

    async sendBannerLocaweb(account: Account, action: actionEnum): Promise<void> {
        try {
            // throw new Error('')

            console.log(format(new Date(), 'dd/MM/yyyy'), `Enviando banner lw ${account.globoId}`);

            const body = [CreateBannerPayloadLocaweb(account, action)];

            console.log('banner payload', body);

            const response = await this.axiosInstance.post(
                `${this.urlLocaWeb}/submit-banner`,
                body
            )

            console.log('banner response', response.data);

            account.bannerSended = true;
            this.accountRepository.update(account.id, account);

            console.log(format(new Date(), 'dd/MM/yyyy'), `Banner Enviado lw ${account.globoId}`);

            return response.data;
        } catch (error) {
            console.log('banner error', error);
            account.bannerSended = false;
            this.accountRepository.update(account.id, account);
            // throw error;
        }
    }

}