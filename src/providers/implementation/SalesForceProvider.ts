import { ISalvesForceProvider } from "../ISalesForceProvider";
import axios from 'axios';
import { LocalStorage } from 'node-localstorage';
import { formatDistanceToNowStrict } from "date-fns";
import { IUsersSalesForceResponseDTO } from "../../utils/parseAccountToSalesforce";
import HttpsProxyAgent from 'https-proxy-agent';
import format from "date-fns/format";

export class SalesForceProvider implements ISalvesForceProvider {
    private salesForceTokenUrl: string;
    private salesForceUrl: string;
    private securityTokenSalesForce: string;
    httpsAgent: any;
    axiosInstance: any;

    private getTokenParams: {
        grant_type: string;
        client_id: string;
        client_secret: string;
        username: string;
        password: string;
    }

    private localStorage: LocalStorage;

    constructor() {
        this.setAxiosProxyAgent();

        this.localStorage = new LocalStorage('./scratch');

        this.salesForceTokenUrl = process.env.TOKEN_URL_SF;
        this.salesForceUrl = process.env.URL_SF;
        this.securityTokenSalesForce = process.env.SF_SECURITY_TOKEN;

        this.getTokenParams = {
            grant_type: 'password',
            client_id: process.env.CLIENT_ID_SF,
            client_secret: process.env.CLIENT_SECRET_SF,
            username: process.env.USERNAME_SF,
            password: process.env.PASSWORD_SF
        }
    }

    setAxiosProxyAgent() {
        if (process.env.PROXY) {
            const [host, port] = process.env.PROXY.split(':');
            this.httpsAgent = HttpsProxyAgent({ host, port });
            this.axiosInstance = axios.create({ httpsAgent: this.httpsAgent })
        } else {
            this.axiosInstance = axios;
        }
    }

    async getHeadersSalesForce() {
        try {
            const {
                grant_type,
                client_id,
                client_secret,
                username,
                password
            } = this.getTokenParams;

            let token: string;
            let salesForceToken = JSON.parse(this.localStorage.getItem('sf_token'));
            if (salesForceToken) {
                const [time, prefix] = formatDistanceToNowStrict(new Date(salesForceToken.expires)).split(' ');
                if (
                    prefix == 'minutes' && parseInt(time) >= 14 ||
                    prefix == 'hours' ||
                    prefix == 'days' ||
                    prefix == 'day'
                ) {
                    const tokenResponse = await this.axiosInstance.post(`${this.salesForceTokenUrl}`, null, {
                        params: {
                            grant_type,
                            client_id,
                            client_secret,
                            username,
                            password: `${password}${this.securityTokenSalesForce}`
                        }
                    });

                    this.localStorage.removeItem('sf_token');
                    this.localStorage.setItem('sf_token', JSON.stringify({
                        token: tokenResponse.data.access_token,
                        expires: new Date()
                    }));

                    token = tokenResponse.data.access_token;
                } else {
                    token = salesForceToken.token;
                }
            } else {
                const tokenResponse = await this.axiosInstance.post(`${this.salesForceTokenUrl}`, null, {
                    params: {
                        grant_type,
                        client_id,
                        client_secret,
                        username,
                        password: `${password}${this.securityTokenSalesForce}`
                    }
                });

                this.localStorage.setItem('sf_token', JSON.stringify({
                    token: tokenResponse.data.access_token,
                    expires: new Date()
                }));

                token = tokenResponse.data.access_token;
            }

            let header = {
                Authorization: `Bearer ${token}`,
                'Content-Type': 'application/json'
            }

            return header;
        } catch (error) {
            console.log('err on get token salesforce -->', error.message);
            throw error;
        }
    }

    async sendToSalesForce(salesforceUser: IUsersSalesForceResponseDTO): Promise<any> {
        try {
            console.log(format(new Date(), 'dd/MM/yyyy'), `Enviando para salesforce ${salesforceUser.globoId}`);
            const config = await this.getHeadersSalesForce();
            const body = {
                globoIds: [salesforceUser]
            }

            const send = await this.axiosInstance.post(
                `${this.salesForceUrl}/api/globomailDiscontinuation/*`,
                body,
                { headers: config }
            );
            console.log(format(new Date(), 'dd/MM/yyyy'), `Enviado para salesforce ${salesforceUser.globoId}`);


            return send;
        } catch (error) {
            console.log('err send to salesforce', error.message);
            throw error;
        }
    }
}