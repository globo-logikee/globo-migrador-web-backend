import { UpdateResult } from "typeorm";
import { Account, PROCCESS_STATUSES, User } from "../entities";

export interface IAccountRepository {
    save(account: Account): Promise<Account>;
    findById(id: string): Promise<Account>;
    update(id: string, account: Account): Promise<UpdateResult>;
    findByUsernameAndServiceId(username: string, serviceId: number): Promise<Account>;
    getByUser(userId: string): Promise<Account>;
    findByGloboId(globoId: string): Promise<Account[]>;
    findByServiceId(serviceId: number): Promise<Account>;
    findByProcessStatus(status: PROCCESS_STATUSES): Promise<Account[]>;
    countAccounts(filter: any): Promise<number>;
}
