import { CommunicationSalesforce } from "../entities/CommunicationSalesforce";

export interface ICommunicationSalesforceRepository {
    save(entity: CommunicationSalesforce): Promise<CommunicationSalesforce>;
    findByGloboId(globoId: string): Promise<CommunicationSalesforce>;
}