import { Compare } from "../entities/Compare";

export interface IComparetRepository {
    findByGloboId(globoId: string): Promise<Compare>;
    create(compare: Compare): Promise<Compare>;
}