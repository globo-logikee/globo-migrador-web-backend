import { Log } from "../entities";

export interface ILogRepository {
    save(log: Log): Promise<Log>;
    findById(id: string): Promise<Log>;
    find(filter: any): Promise<Log[]>;
    count(filter: any): Promise<number>;
}