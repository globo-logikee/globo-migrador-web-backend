import { UpdateResult } from "typeorm";
import { User } from "../entities";

export interface IUserRepository {
    save(user: User): Promise<User>;
    findById(id: string): Promise<User>;
    finbByGloboId(globoId: string): Promise<User>;
    update(id: string, user: User): Promise<UpdateResult>;
    countUsers(filter?: any): Promise<number>;
    find(filter: any): Promise<User[]>
}