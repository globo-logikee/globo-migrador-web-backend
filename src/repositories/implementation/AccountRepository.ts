import { getRepository, UpdateResult, getMongoRepository } from 'typeorm';

import { Account, PROCCESS_STATUSES, User } from "../../entities";
import { IAccountRepository } from "../IAccountRepository";

export class AccountRepository implements IAccountRepository {
    async findByUsernameAndServiceId(username: string, serviceId: number): Promise<Account> {
        return await getRepository(Account).findOne({ username, serviceId });
    }
    async findById(id: string): Promise<Account> {
        return await getRepository(Account).findOne(id);
    }
    async save(account: Account): Promise<Account> {
        return await getRepository(Account).save(account);
    }
    async update(id: string, account: Account): Promise<UpdateResult> {
        return await getRepository(Account).update(id, account);
    }
    async getByUser(userId: string): Promise<Account> {
        return await getMongoRepository(Account).findOne({ userId })
    }
    async findByGloboId(globoId: string): Promise<Account[]> {
        return await getRepository(Account).find({ globoId });
    }
    async findByServiceId(serviceId: number): Promise<Account> {
        return await getRepository(Account).findOne({ serviceId });
    }
    async findByProcessStatus(status: PROCCESS_STATUSES): Promise<Account[]> {
        return await getRepository(Account).find({ processStatus: status });
    }
    async countAccounts(filter: any): Promise<number> {
        return await getRepository(Account).count(filter);
    }
}