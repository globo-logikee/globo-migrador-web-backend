import { CommunicationSalesforce } from "../../entities/CommunicationSalesforce";
import { getRepository } from "typeorm";
import { ICommunicationSalesforceRepository } from "../ICommunicationSalesforceRepository";

export class CommunicationSalesforceRepository implements ICommunicationSalesforceRepository {
    async save(entity: CommunicationSalesforce): Promise<CommunicationSalesforce> {
        return await getRepository(CommunicationSalesforce).save(entity);
    }
    async findByGloboId(globoId: string): Promise<CommunicationSalesforce> {
        return await getRepository(CommunicationSalesforce).findOne({ globoId });
    }

}