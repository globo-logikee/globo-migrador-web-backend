import { Compare } from "../../entities/Compare";
import { getRepository } from "typeorm";
import { IComparetRepository } from "../ICompareRepository";

export class CompareRepository implements IComparetRepository {
    async create(compare: Compare): Promise<Compare> {
        return await getRepository(Compare).save(compare);
    }
    async findByGloboId(globoId: string): Promise<Compare> {
        return await getRepository(Compare).findOne({ globoId });
    }
}