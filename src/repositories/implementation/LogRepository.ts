import { getRepository } from 'typeorm';

import { Log } from "../../entities";
import { ILogRepository } from "../ILogRepository";

export class LogRepository implements ILogRepository {
    findById(id: string): Promise<Log> {
        throw new Error('Method not implemented.');
    }
    async save(log: Log): Promise<Log> {
        return await getRepository(Log).save(log);
    }
    async find(filter: any): Promise<Log[]> {
        return await getRepository(Log).find(filter);
    }
    async count(filter: any): Promise<number> {
        return await getRepository(Log).count(filter);
    }
}