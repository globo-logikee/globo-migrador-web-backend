import { getConnection, getRepository, UpdateResult } from 'typeorm';

import { User } from "../../entities";
import { IUserRepository } from "../IUserRepository";

export class UserRepository implements IUserRepository {
    async update(id: string, user: User): Promise<UpdateResult> {
        return await getRepository(User).update(id, user);
    }
    async finbByGloboId(globoId: string): Promise<User> {
        return await getRepository(User).findOne({ globoId });
    }
    async findById(id: string): Promise<User> {
        return await getRepository(User).findOne(id);
    }
    async save(user: User): Promise<User> {
        return await getRepository(User).save(user);
    }
    async countUsers(filter: any): Promise<number> {
        return await getRepository(User).count(filter);
    }
    async find(filter: any): Promise<User[]> {
        return await getRepository(User).find(filter);
    }
}