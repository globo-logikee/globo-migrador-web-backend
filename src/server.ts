import 'reflect-metadata';
import './database/connection';
import api from './api/api';

const port = process.env.PORT || '3003';
api.listen(port, () => console.log('💻 server started'));