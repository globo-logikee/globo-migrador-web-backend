import { Request, Response } from "express";
import { ChooserUseCase } from "./ChooserUseCase";

export class ChooserController {
    constructor(
        private chooserUseCase: ChooserUseCase
    ) { }

    async handle(req: Request, res: Response): Promise<Response> {
        const { accountId, globoId } = req.params;

        try {
            const response = await this.chooserUseCase.execute({
                accountId,
                globoId
            });

            return res.json(response);
        } catch (error) {
            const { status, message } = error;

            return res.status(status ? status : 500).json({
                statusCode: status,
                message: message
            });
        }
    }
}