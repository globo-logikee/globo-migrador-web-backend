import { IAccountRepository } from "../../repositories/IAccountRepository";
import { IUserRepository } from "../../repositories/IUserRepository";
import { IGloboIdApiProvider } from "../../providers/IGloboIdApiProvider";
import { IChooserDTO } from "./IChooserDTO";
import { ILogRepository } from "../../repositories/ILogRepository";
import { Log, PROCCESS_MESSAGES, PROCCESS_STATUSES } from "../../entities";

import errors from 'http-errors';
import { IComparetRepository } from "../../repositories/ICompareRepository";
import { add, format } from 'date-fns';

export class ChooserUseCase {
    private addDaysToComunicationDate: number;

    constructor(
        private userRepository: IUserRepository,
        private accountRepository: IAccountRepository,
        private logRepository: ILogRepository,
        private compareRepository: IComparetRepository,
        private globoIdApiProvider: IGloboIdApiProvider
    ) {
        this.addDaysToComunicationDate = parseInt(process.env.ADD_DAYS_TO_COMUNICATION_DATE);
    }

    async execute(data: IChooserDTO) {
        const { accountId, globoId } = data;

        const user = await this.userRepository.finbByGloboId(globoId);
        if (!user) {
            throw new errors.NotFound('user_not_found');
        }

        const userAccount = await this.accountRepository.findById(accountId);
        if (!userAccount) {
            throw new errors.NotFound('account_not_found');
        }

        // const findByUser = await this.accountRepository.getByUser(user);
        // if (!findByUser) {
        //     throw new errors.Unauthorized('unauthorized');
        // }

        delete userAccount.lote;
        delete userAccount.docType;
        delete userAccount.address;
        delete userAccount.bannerSended;

        if (userAccount.processStatus !== PROCCESS_STATUSES.CREATED && userAccount.processStatus !== PROCCESS_STATUSES.SALESFORCE_INPUT) {
            return userAccount;
        }

        await this.logRepository.save(new Log({
            globoId: user.globoId,
            username: user.username,
            userIp: userAccount.userIp ? userAccount.userIp : null,
            message: PROCCESS_MESSAGES.SELECTED_ACCOUNT(userAccount.email)
        }));

        // --> Atualiza status da conta para "Aguardando confirmação"
        userAccount.processStatus = PROCCESS_STATUSES.WAITING_USER_CONFIRM;
        userAccount.status_opt = 3;

        // --> Se service 1 mantem username do email globo e atualiza conta
        if (userAccount.serviceId == 1) {
            userAccount.newEmail = `${userAccount.email.split('@')[0]}@lwmail.com.br`;
            await this.accountRepository.update(userAccount.id, userAccount);
            delete userAccount.password;

            await this.logRepository.save(new Log({
                globoId: user.globoId,
                username: user.username,
                userIp: userAccount.userIp ? userAccount.userIp : null,
                message: PROCCESS_MESSAGES.NEW_ACCOUNT_LOCALWEB(userAccount.newEmail)
            }));

            return userAccount;
        }

        // --> Se service 1948 ultiliza tabela de depara
        const emailSuggestion = await this.compareRepository.findByGloboId(globoId);
        // --> Se não existir email na tabela mantem username do email globo
        if (!emailSuggestion) {
            const accounts = await this.accountRepository.findByGloboId(globoId);
            if (accounts.length == 1) {
                userAccount.newEmail = `${userAccount.email.split('@')[0]}@lwmail.com.br`;
            } else {
                userAccount.newEmail = `${userAccount.email.split('@')[0]}-1948@lwmail.com.br`;
            }

            await this.accountRepository.update(userAccount.id, userAccount);
            delete userAccount.password;

            await this.logRepository.save(new Log({
                globoId: user.globoId,
                username: user.username,
                userIp: userAccount.userIp ? userAccount.userIp : null,
                message: PROCCESS_MESSAGES.NEW_ACCOUNT_LOCALWEB(userAccount.newEmail)
            }));

            return userAccount;
        }

        // --> Se existir email na tabela atualiza conta
        userAccount.newEmail = `${emailSuggestion.lwmail}@lwmail.com.br`;
        await this.accountRepository.update(userAccount.id, userAccount);
        delete userAccount.password;

        await this.logRepository.save(new Log({
            globoId: user.globoId,
            username: user.username,
            userIp: userAccount.userIp ? userAccount.userIp : null,
            message: PROCCESS_MESSAGES.NEW_ACCOUNT_LOCALWEB(userAccount.newEmail)
        }));

        return userAccount;
    }
}