import { ChooserController } from "./ChooserController";
import { ChooserUseCase } from "./ChooserUseCase";
import { GloboIdApiProvider } from '../../providers/implementation';
import { AccountRepository, UserRepository, LogRepository, CompareRepository } from '../../repositories/implementation';

const compareRepository = new CompareRepository();
const logRepository = new LogRepository();
const accountRepository = new AccountRepository();
const userRepository = new UserRepository();
const globoIdApiProvider = new GloboIdApiProvider();
const chooserUseCase = new ChooserUseCase(userRepository, accountRepository, logRepository, compareRepository, globoIdApiProvider);
const chooserController = new ChooserController(chooserUseCase);

export { chooserController };