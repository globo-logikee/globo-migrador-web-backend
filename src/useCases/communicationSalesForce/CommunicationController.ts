import { Request, Response } from "express";
import { CommunicationUseCase } from "./CommunicationUseCase";

export class CommunicationController {
    constructor(
        private communicationUseCase: CommunicationUseCase
    ) { }

    async handle(req: Request, res: Response): Promise<Response> {
        const { globoIds } = req.body;

        try {
            const response = await this.communicationUseCase.execute({
                globoIds
            });

            return res.json(response);
        } catch (error) {
            console.log(error);
            const { status, message } = error;

            return res.status(status ? status : 500).json(message);
        }
    }
}