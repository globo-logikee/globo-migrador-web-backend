import { format, add, isValid } from "date-fns";
import { mapSeries } from "p-iteration";
import { Account, Log, PROCCESS_MESSAGES, PROCCESS_STATUSES, User } from "../../entities";
import { IGloboIdApiProvider } from "../../providers/IGloboIdApiProvider";
import { IAccountRepository } from "../../repositories/IAccountRepository";
import { IUserRepository } from "../../repositories/IUserRepository";
import { CommunicationSalesforce } from "../../entities/CommunicationSalesforce";
import { IComunicationDTO } from "./ICommunicationDTO";

import errors from 'http-errors';
import { ILogRepository } from "../../repositories/ILogRepository";
import { IHasherProvider } from "../../providers/IHasherProvider";
import { SmartInterventionKeys, SmartInterventionValues } from "../../providers/implementation";
import { ISalvesForceProvider } from "../../providers/ISalesForceProvider";
import { ParseAccountToSalesForce } from "../../utils/parseAccountToSalesforce";
import { ILocaWebProvider } from "../../providers/ILocaWebProvider";
import { actionEnum } from "../../utils/createBannerPayloLocaweb";

export class CommunicationUseCase {
    private addDaysToCommunicationDate: number

    constructor(
        private salesForceProvider: ISalvesForceProvider,
        private globoIdApiProvider: IGloboIdApiProvider,
        private accountRepository: IAccountRepository,
        private userRepository: IUserRepository,
        private logRepository: ILogRepository,
        private hashProvider: IHasherProvider,
        private locawebProvider: ILocaWebProvider
    ) {
        this.addDaysToCommunicationDate = parseInt(process.env.ADD_DAYS_TO_COMUNICATION_DATE);
    }

    async execute(data: IComunicationDTO) {
        let globoIds = [];
        await mapSeries(data.globoIds, async globoId => {
            if (!globoId.globoId) {
                throw new errors.BadRequest('globoid_uninformed');
            }

            if (!globoId.serviceId) {
                throw new errors.BadRequest('serviceId_uninformed');
            }

            if (!globoId.firstCommunicationDate) {
                throw new errors.BadRequest('firstCommunicationDate_uninformed');
            }

            try {
                if (!isValid(new Date(globoId.firstCommunicationDate))) {
                    throw new Error('Data da primeira comunicação mal formatada');
                }

                let {
                    username,
                    contact,
                    globocomEmail,
                    globomailEmail,
                    fullName,
                    email,
                    docNumber,
                    phones,
                    address,
                    alternativeEmail,
                    docType
                } = await this.globoIdApiProvider.getUserData(globoId.globoId);

                let user = await this.userRepository.finbByGloboId(globoId.globoId);
                if (!user) {
                    user = await this.userRepository.save(new User({
                        globoId: globoId.globoId,
                        fullName,
                        contactEmail: contact,
                        username,
                        alternativeEmail,
                        serviceIds: [{
                            firstCommunication: globoId.firstCommunicationDate,
                            serviceId: parseInt(globoId.serviceId)
                        }]
                    }));
                } else {
                    const service = user.serviceIds.find(service => service.serviceId == parseInt(globoId.serviceId));
                    if (!service) {
                        user.serviceIds.push({
                            firstCommunication: globoId.firstCommunicationDate,
                            serviceId: parseInt(globoId.serviceId)
                        });

                        await this.userRepository.update(user.id, user);
                    }
                }


                let account = await this.accountRepository.findByUsernameAndServiceId(user.username, parseInt(globoId.serviceId));
                if (!account) {
                    // --> Gera data da migração 
                    const migrationDate = add(
                        new Date(globoId.firstCommunicationDate),
                        { days: this.addDaysToCommunicationDate }
                    );

                    const scheduledTo = format(add(
                        new Date(globoId.firstCommunicationDate),
                        { days: this.addDaysToCommunicationDate }
                    ), 'yyyy-MM-dd');

                    if (phones) {
                        if (phones.cellPhone) {
                            phones.cellPhone = await this.hashProvider.encrypt(phones.cellPhone);
                        }
                        if (phones.phone) {
                            phones.phone = await this.hashProvider.encrypt(phones.phone);
                        }
                    }

                    if (docNumber) {
                        docNumber = await this.hashProvider.encrypt(docNumber);
                    }

                    const globoUserServices = await this.globoIdApiProvider.getUserServices(globoId.globoId);
                    let services = [];
                    for (let service of globoUserServices) {
                        services.push(service.serviceId);
                    }
                    let serviceAlreadyExists: boolean;
                    if (services.includes(parseInt(globoId.serviceId))) {
                        serviceAlreadyExists = true;
                    } else {
                        serviceAlreadyExists = false;
                    }

                    account = await this.accountRepository.save(new Account({
                        processStatus: PROCCESS_STATUSES.SALESFORCE_INPUT,
                        globoId: user.globoId,
                        fullName: user.fullName,
                        docNumber,
                        username,
                        serviceId: parseInt(globoId.serviceId),
                        firstCommunicationDate: globoId.firstCommunicationDate,
                        phones,
                        docType,
                        globoMail: email,
                        status_opt: 3,
                        lote: globoId.lote,
                        scheduledTo,
                        migrationDate,
                        serviceAlreadyExists,
                        contactEmail: user.contactEmail,
                        alternativeEmail: user.alternativeEmail,
                        address,
                        email: parseInt(globoId.serviceId) == 1 ? globocomEmail : globomailEmail,
                        user,
                        userId: user.id
                    }));

                    await this.logRepository.save(new Log({
                        globoId: user.globoId,
                        username: user.username,
                        serviceId: account.serviceId,
                        message: PROCCESS_MESSAGES.MIGRATION_DATE(format(add(
                            new Date(globoId.firstCommunicationDate),
                            { days: this.addDaysToCommunicationDate }
                        ), 'yyyy-MM-dd').toString())
                    }));

                    const {
                        smart_intervention_globomail_free,
                        smart_intervention_globomail_pro
                    } = SmartInterventionKeys;
                    const {
                        descontinuacao_globomail_free,
                        descontinuacao_globomail_pro
                    } = SmartInterventionValues;
                    await this.globoIdApiProvider.insertSmartIntervention(
                        account.serviceId == 1 ? smart_intervention_globomail_pro : smart_intervention_globomail_free,
                        account.serviceId == 1 ? descontinuacao_globomail_pro : descontinuacao_globomail_free,
                        account.globoId
                    );

                    await this.salesForceProvider.sendToSalesForce(ParseAccountToSalesForce(account));
                    await this.locawebProvider.sendBannerLocaweb(account, actionEnum.communication);
                } else {
                    globoIds.push({
                        globoId: globoId.globoId,
                        serviceId: globoId.serviceId,
                        erro: {
                            code: 403,
                            reason: 'Service and user already existing in the database'
                        }
                    });
                }
            } catch (error) {
                const { message } = error;

                const messages = [
                    'Request failed with status code 404',
                ]
                if (messages.includes(message)) {
                    if (message == 'Request failed with status code 404') {
                        globoIds.push({
                            globoId: globoId.globoId,
                            serviceId: globoId.serviceId,
                            erro: {
                                code: 404,
                                reason: 'User not found on GloboID API'
                            }
                        });
                    }
                } else {
                    globoIds.push({
                        globoId: globoId.globoId,
                        serviceId: globoId.serviceId,
                        erro: {
                            code: 500,
                            reason: message
                        }
                    });
                }
            }
        });



        if (globoIds.length > 0) {
            return { globoIds };
        }

        return;
    }
}