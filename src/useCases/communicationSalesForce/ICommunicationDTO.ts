interface globoInfo {
    firstCommunicationDate: string;
    globoId: string;
    serviceId: string;
    lote: string;
}

export interface IComunicationDTO {
    globoIds: globoInfo[]
}