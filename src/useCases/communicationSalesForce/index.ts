import { CommunicationController } from './CommunicationController';
import { CommunicationUseCase } from './CommunicationUseCase';
import { UserRepository, AccountRepository, LogRepository } from '../../repositories/implementation';
import { GloboIdApiProvider, BcryptProvider, SalesForceProvider, LocaWebProvider } from '../../providers/implementation';

const bcryptProvider = new BcryptProvider();
const globoIdApiProvider = new GloboIdApiProvider();
const logRepository = new LogRepository()
const userRepository = new UserRepository();
const accountRepository = new AccountRepository();
const salesForceProvider = new SalesForceProvider();
const locawebProvider = new LocaWebProvider();
const firstCommunicationUseCase = new CommunicationUseCase(salesForceProvider, globoIdApiProvider, accountRepository, userRepository, logRepository, bcryptProvider, locawebProvider);
const firstCommunicationController = new CommunicationController(firstCommunicationUseCase);

export { firstCommunicationController };