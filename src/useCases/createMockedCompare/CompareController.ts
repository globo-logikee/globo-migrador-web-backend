import { Request, Response } from "express";
import { CompareUseCase } from "./CompareUseCase";

export class CompareController {
    constructor(
        private compareUseCase: CompareUseCase
    ) { }

    async handle(req: Request, res: Response): Promise<Response> {
        const { compares } = req.body;

        try {
            return res.json(
                await this.compareUseCase.execute({ compares })
            );
        } catch (error) {
            const { status, message } = error;
            
            return res.status(status ? status : 500).json({
                code: status,
                reason: message
            });
        }
    }
}