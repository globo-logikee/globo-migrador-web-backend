import { mapSeries } from "p-iteration";
import { Compare } from "../../entities/Compare";
import { IComparetRepository } from "../../repositories/ICompareRepository";
import { ICompareDTO } from "./ICompareDTO";

import errors from 'http-errors';

export class CompareUseCase {
    constructor(
        private compareRepository: IComparetRepository
    ) { }

    async execute(data: ICompareDTO) {
        let comparesCreated = [];
        let comparesMalFormated = [];

        await mapSeries(data.compares, async (compare) => {
            if (compare.lwmail.indexOf('@') > -1) {
                comparesMalFormated.push(compare);
            } else {
                comparesCreated.push(
                    await this.compareRepository.create(new Compare({ ...compare }))
                );
            }
        });

        if (comparesMalFormated.length > 0) {
            throw {
                status: 400,
                message: { comparesMalFormated }
            }
        }

        return comparesCreated;
    }
}