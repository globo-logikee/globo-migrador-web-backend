interface compare {
    globoId: string;
    globoMailFree: string;
    lwmail: string;
}

export interface ICompareDTO {
    compares: compare[];
}