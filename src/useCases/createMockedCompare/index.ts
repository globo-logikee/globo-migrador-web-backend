import { CompareController } from './CompareController';
import { CompareUseCase } from './CompareUseCase';
import { CompareRepository } from '../../repositories/implementation';

const compareRepository = new CompareRepository();
const compareUseCase = new CompareUseCase(compareRepository);
const compareController = new CompareController(compareUseCase);

export { compareController };