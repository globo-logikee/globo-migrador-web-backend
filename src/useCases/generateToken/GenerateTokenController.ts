import { Request, Response } from "express";
import { GenerateTokenUseCase } from "./GenerateTokenUseCase";

export class GenerateTokenController {
    constructor(
        private generateTokenUseCase: GenerateTokenUseCase
    ) { }

    async handle(req: Request, res: Response): Promise<Response> {
        const { globoId } = req.body;

        try {
            const response = await this.generateTokenUseCase.execute({
                globoId
            });

            return res.json(response);
        } catch (error) {
            console.log(error);
            const { status, message } = error;

            return res.status(status ? status : 500).json({
                statusCode: status,
                message: message
            });
        }
    }
}