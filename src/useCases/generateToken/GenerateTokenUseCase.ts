import { IGloboIdApiProvider } from "../../providers/IGloboIdApiProvider";
import { ITokenProvider } from "../../providers/ITokenProvider";
import { IAccountRepository } from "../../repositories/IAccountRepository";
import { IGenerateToken } from "./IGenerateToken";

import errors from 'http-errors';
import { IUserRepository } from "../../repositories/IUserRepository";

export class GenerateTokenUseCase {
    constructor(
        private globoIdProvider: IGloboIdApiProvider,
        private userRepository: IUserRepository,
        private tokenProvider: ITokenProvider,
    ) { }

    async execute(data: IGenerateToken) {
        const { globoId } = data;

        try {
            const globoUser = await this.globoIdProvider.getUserData(globoId);
            if (!globoUser) {
                throw new errors.Unauthorized('globouser_not_found');
            }

            const mappedServicesIds = [1, 1948];
            const globoUserServices = await this.globoIdProvider.getUserServices(globoId);
            const servicesExists = globoUserServices.filter(arr => {
                return mappedServicesIds.includes(arr.serviceId)
            });
            const user = await this.userRepository.finbByGloboId(globoId);
            if (!user && servicesExists.length == 0) {
                throw new errors.Unauthorized('dont_have_globomail');
            }
            if (!user) {
                throw new errors.Unauthorized('email_not_able_to_opt');
            }

            return { token: await this.tokenProvider.generate({ globoId, username: user.username }) };
        } catch (error) {
            const { message } = error;

            if (message == 'Request failed with status code 404') {
                throw new errors.Unauthorized('globouser_not_found');
            }

            throw error;
        }


    }
}