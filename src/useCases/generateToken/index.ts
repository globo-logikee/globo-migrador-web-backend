import { GenerateTokenController } from './GenerateTokenController';
import { GenerateTokenUseCase } from './GenerateTokenUseCase';
import { GloboIdApiProvider, JwtProvider } from '../../providers/implementation';
import { UserRepository } from '../../repositories/implementation';

const userRepository = new UserRepository();
const jwtProvider = new JwtProvider();
const globoIdProvider = new GloboIdApiProvider();
const generateTokenUseCase = new GenerateTokenUseCase(globoIdProvider, userRepository, jwtProvider);
const generateTokenController = new GenerateTokenController(generateTokenUseCase);

export { generateTokenController };