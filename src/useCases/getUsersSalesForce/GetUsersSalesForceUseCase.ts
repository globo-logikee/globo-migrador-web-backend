import { IAccountRepository } from "../../repositories/IAccountRepository";
import { IGetUsersSalesForceRequestDTO, IGetUsersSalesForceResponseDTO } from "./IGetUsersSalesForceDTO";
import { PROCCESS_STATUSES } from "../../entities";

import { mapSeries } from "p-iteration";
import { isAfter } from "date-fns";
import { ParseAccountToSalesForceGet } from "../../utils/parseAccountToSalesforce";


export class GetUsersSalesForceUseCase {
    constructor(
        private accountRepository: IAccountRepository,
    ) { }

    async execute(data: IGetUsersSalesForceRequestDTO) {
        const { globoIds } = data;

        let response = { globoIds: [] };
        await mapSeries(globoIds, async globoId => {
            let userResponse: IGetUsersSalesForceResponseDTO;

            const accounts = await this.accountRepository.findByGloboId(globoId);
            if (!accounts.length) {
                userResponse = {
                    globoId,
                    erro: {
                        code: 404,
                        reason: 'user not found'
                    }
                }

                response.globoIds.push(userResponse);
            } else {
                await mapSeries(accounts, async account => {
                    userResponse = ParseAccountToSalesForceGet(account);
                    response.globoIds.push(userResponse);
                });
            }
        });

        return response;
    }
}