export interface IGetUsersSalesForceRequestDTO {
    globoIds: string[]
}

export interface IGetUsersSalesForceResponseDTO {
    globoId: string;
    email?: string;
    username?: string;
    status_opt?: number;
    decisionDate?: string;
    firstCommunicationDate?: string;
    scheduledTo?: string;
    effectiveActionDate?: string;
    globoIdDeactivationDate?: string;
    action?: number;
    changedDecision?: string;
    changedDecisionDate?: string;
    serviceId?: number;
    lote?: string;
    erro?: {
        code: number,
        reason: string
    }
}