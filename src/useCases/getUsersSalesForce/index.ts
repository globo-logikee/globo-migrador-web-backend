import { GetUsersSalesForceController } from './GetUsersSalesForceController';
import { GetUsersSalesForceUseCase } from './GetUsersSalesForceUseCase';
import { AccountRepository } from '../../repositories/implementation';

const accountRepository = new AccountRepository();
const getUsersSalesForceUseCase = new GetUsersSalesForceUseCase(accountRepository);
const getUsersSalesForceController = new GetUsersSalesForceController(getUsersSalesForceUseCase);

export { getUsersSalesForceController };