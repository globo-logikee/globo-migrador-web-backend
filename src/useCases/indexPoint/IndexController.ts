import { Request, Response } from "express";
import { IndexUseCase } from "./IndexUseCase";

export class IndexController {
    constructor(
        private indexUseCase: IndexUseCase
    ) { }

    async handle(req: Request, res: Response): Promise<Response> {
        const { globoId, glbId, userIp } = req.body;
        const token = req.header('Authorization');;

        try {
            const response = await this.indexUseCase.execute({
                globoId,
                glbId,
                userIp
            }, token);

            return res.json(response);
        } catch (error) {
            console.log(error.message);
            const { status, message } = error;

            return res.status(status ? status : 500).json({
                statusCode: status,
                message: message
            });
        }
    }
}