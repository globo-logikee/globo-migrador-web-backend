import { IGloboIdApiProvider } from "../../providers/IGloboIdApiProvider";
import { IUserRepository } from "../../repositories/IUserRepository";
import { IIndexDTO } from "./IIndexDTO";
import { IIpGetterProvider } from "../../providers/IIpGetterProvider";
import { PROCCESS_STATUSES } from '../../entities/Account';
import { PROCCESS_MESSAGES } from '../../entities/Log';
import { IAccountRepository } from "../../repositories/IAccountRepository";
import { ILogRepository } from "../../repositories/ILogRepository";
import { User, Account, Log } from "../../entities";
import { ISalvesForceProvider } from "../../providers/ISalesForceProvider";

import errors from 'http-errors';
import { mapSeries } from 'p-iteration';
import axios from 'axios';
import { add, formatDistanceToNowStrict, isAfter } from 'date-fns';
import format from "date-fns/format";
import addISOWeekYears from "date-fns/esm/addISOWeekYears/index";
import { IHasherProvider } from "../../providers/IHasherProvider";

export class IndexUseCase {
    private myUrl: string;
    private comunicationDateExpiresInDays: number;
    private addDaysToComunicationDate: number;

    constructor(
        private userRepository: IUserRepository,
        private accountRepository: IAccountRepository,
        private logRepository: ILogRepository,
        private globoIdApiProvider: IGloboIdApiProvider,
        private hasherProvider: IHasherProvider
    ) {
        this.addDaysToComunicationDate = parseInt(process.env.ADD_DAYS_TO_COMUNICATION_DATE)
        this.myUrl = process.env.MY_URL;
        this.comunicationDateExpiresInDays = parseInt(process.env.MOCK_SALESFORCE_EXPIRES_IN_DAYS)
    }

    async execute(data: IIndexDTO, token: string) {
        // --> Busca usuario a partir do globo id vindo do frontend
        const globoUser = await this.globoIdApiProvider.getUserData(data.globoId);
        const {
            username,
            contact,
            globoId,
            globocomEmail,
            globomailEmail,
            fullName,
            docNumber,
            phones,
            address,
            docType,
            email
        } = globoUser;

        //--> Busca os serviços do usuario
        const mappedServicesIds = [1, 1948];
        const globoUserServices = await this.globoIdApiProvider.getUserServices(data.globoId);
        const servicesExists = globoUserServices.filter(arr => {
            return mappedServicesIds.includes(arr.serviceId)
        });

        // --> Cria usuario no db
        let user = await this.userRepository.finbByGloboId(globoUser.globoId);
        if (!user && servicesExists.length == 0) {
            throw new errors.Unauthorized('dont_have_globomail');
        }
        if (!user) {
            throw new errors.Unauthorized('email_not_able_to_opt');
        }

        user.accessed = true;
        await this.userRepository.update(user.id, user);

        const accounts = await this.accountRepository.findByGloboId(user.globoId);
        let expiredAccounts: Account[] = [];
        let noExpiredAccounts: Account[] = [];
        await mapSeries(accounts, async account => {
            console.log(account.scheduledTo)

            const isExpired = isAfter(
                new Date(),
                new Date(account.scheduledTo)
            );
            if (format(new Date(), 'yyyy-MM-dd') == account.scheduledTo || isExpired) {
                await this.logRepository.save(new Log({
                    globoId,
                    username,
                    serviceId: account.serviceId,
                    userIp: data.userIp ? data.userIp : null,
                    message: PROCCESS_MESSAGES.EXPIRED
                }));

                expiredAccounts.push(account);
            } else {
                noExpiredAccounts.push(account);
            }
        });

        if (expiredAccounts.length > 0) {
            if (noExpiredAccounts.length == 0) {
                if (expiredAccounts.length == 1) {
                    if (expiredAccounts[0].processStatus != PROCCESS_STATUSES.OPT_IN_DONE) {
                        throw new errors.Unauthorized(`deadline/${expiredAccounts[0].scheduledTo}`);
                    }
                }
            }
        }

        await this.logRepository.save(new Log({
            globoId,
            username,
            userIp: data.userIp ? data.userIp : null,
            message: PROCCESS_MESSAGES.USER_LOGIN(username)
        }));


        // --> Verifica a "autorização" dos serviços
        let validAccounts = [];
        await mapSeries(globoUserServices, async service => {
            const { serviceId } = service;

            if (mappedServicesIds.includes(serviceId)) {
                const account = await this.accountRepository.findByUsernameAndServiceId(
                    user.username,
                    serviceId
                );
                if (account) {
                    if (account.processStatus == PROCCESS_STATUSES.SALESFORCE_INPUT) {
                        account.processStatus = PROCCESS_STATUSES.CREATED;
                        account.userIp = data.userIp ? data.userIp : null;
                        await this.accountRepository.update(account.id, account);
                    }

                    if (account.password) {
                        account.length = (await this.hasherProvider.decrypt(account.password)).length;
                        account.password = undefined;
                    }

                    delete account.lote;
                    delete account.docType;
                    delete account.address;
                    delete account.bannerSended;

                    validAccounts.push(account);
                }
            }
        });

        if (validAccounts.length > 1) {
            return validAccounts;
        } else if (validAccounts.length == 1) {
            const accounts = await this.accountRepository.findByGloboId(user.globoId);
            if (accounts.length > 1) {
                const account = accounts.find(account => account.serviceId != validAccounts[0].serviceId);

                if (account.processStatus != PROCCESS_STATUSES.SALESFORCE_INPUT || accounts[0].serviceAlreadyExists) {
                    account.userIp = data.userIp ? data.userIp : null;
                    await this.accountRepository.update(account.id, account);

                    if (account.password) {
                        account.length = (await this.hasherProvider.decrypt(account.password)).length;
                        account.password = undefined;
                    }

                    delete account.lote;
                    delete account.docType;
                    delete account.address;
                    delete account.bannerSended;

                    validAccounts.push(account);
                }

                if (validAccounts.length > 1) {
                    return validAccounts;
                }
            }

            const parts = token.split(' ');
            const [scheme, tk] = parts;
            const { id } = validAccounts[0];


            if (validAccounts[0].processStatus == PROCCESS_STATUSES.SALESFORCE_INPUT) {
                validAccounts[0].processStatus = PROCCESS_STATUSES.CREATED;
                validAccounts[0].userIp = data.userIp ? data.userIp : null;
                await this.accountRepository.update(validAccounts[0].id, validAccounts[0]);
            }

            delete validAccounts[0].lote;
            delete validAccounts[0].docType;
            delete validAccounts[0].address;
            delete validAccounts[0].bannerSended;

            return await (
                await axios.post(
                    `${this.myUrl}/api/chooser/${id}/${user.globoId}`,
                    {},
                    { headers: { Authorization: `Bearer ${tk}` } }
                )
            ).data;
        } else {
            let validAccounts = [];
            const accounts = await this.accountRepository.findByGloboId(user.globoId);

            if (accounts.length > 1) {
                await mapSeries(accounts, async account => {
                    if (account.processStatus != PROCCESS_STATUSES.SALESFORCE_INPUT || accounts[0].serviceAlreadyExists) {
                        account.userIp = data.userIp ? data.userIp : null;
                        await this.accountRepository.update(account.id, account);

                        if (account.password) {
                            account.length = (await this.hasherProvider.decrypt(account.password)).length;
                            account.password = undefined;
                        }

                        delete account.lote;
                        delete account.docType;
                        delete account.address;
                        delete account.bannerSended;

                        validAccounts.push(account);
                    }
                });

                if (validAccounts.length > 1) {
                    return validAccounts;
                } else if (validAccounts.length == 1) {
                    return validAccounts[0];
                }

                throw new errors.Unauthorized('dont_have_globomail');
            } else if (accounts.length == 1) {
                const parts = token.split(' ');
                const [scheme, tk] = parts;
                const { id } = accounts[0];

                if (accounts[0].processStatus != PROCCESS_STATUSES.SALESFORCE_INPUT || accounts[0].serviceAlreadyExists) {
                    accounts[0].userIp = data.userIp ? data.userIp : null;
                    await this.accountRepository.update(accounts[0].id, accounts[0]);

                    if (accounts[0].password) {
                        accounts[0].length = (await this.hasherProvider.decrypt(accounts[0].password)).length;
                        accounts[0].password = undefined;
                    }

                    delete accounts[0].lote;
                    delete accounts[0].docType;
                    delete accounts[0].address;
                    delete accounts[0].bannerSended;

                    return await (
                        await axios.post(
                            `${this.myUrl}/api/chooser/${id}/${user.globoId}`,
                            {},
                            { headers: { Authorization: `Bearer ${tk}` } }
                        )
                    ).data;
                }
            }

            throw new errors.Unauthorized('dont_have_globomail');
        }
    }
}