import { UserRepository, AccountRepository, LogRepository } from '../../repositories/implementation';
import { GloboIdApiProvider, BcryptProvider } from '../../providers/implementation';
import { IndexUseCase } from './IndexUseCase';
import { IndexController } from './IndexController';

const bcryptProvider = new BcryptProvider();
const userRepository = new UserRepository();
const globoIdApiProvider = new GloboIdApiProvider();;
const accountRepository = new AccountRepository();
const logRepository = new LogRepository();
const indexUseCase = new IndexUseCase(
    userRepository,
    accountRepository,
    logRepository,
    globoIdApiProvider,
    bcryptProvider
);
const indexController = new IndexController(indexUseCase);

export { indexController };