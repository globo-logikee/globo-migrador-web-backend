interface Request {
    globoId: string;
    serviceId: number;
}

export interface IReactivateUsersSalesForceDTO {
    globoids: Request[]
}