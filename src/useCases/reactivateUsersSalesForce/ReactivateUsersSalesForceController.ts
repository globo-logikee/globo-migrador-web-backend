import { Request, Response } from "express";
import { ReactivateUsersSalesForceUseCase } from "./ReactivateUsersSalesForceUseCase";

export class ReactivateUsersSalesForceController {
    constructor(
        private reactivateUsersSalesForceUseCase: ReactivateUsersSalesForceUseCase
    ) { }

    async handle(req: Request, res: Response): Promise<Response> {
        const { globoids } = req.body;

        try {
            const response = await this.reactivateUsersSalesForceUseCase.execute({
                globoids
            });

            return res.json(response);
        } catch (error) {
            console.log(error.message);
            const { status, message } = error;

            return res.status(status ? status : 500).json({
                code: status ? status : 500,
                reason: message
            });
        }
    }
}