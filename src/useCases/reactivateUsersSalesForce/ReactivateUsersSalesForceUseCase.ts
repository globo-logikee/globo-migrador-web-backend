import { mapSeries } from "p-iteration";
import { IAccountRepository } from "../../repositories/IAccountRepository";
import { IReactivateUsersSalesForceDTO } from "./IReactivateUsersSalesForceDTO";

import errors from 'http-errors';
import { PROCCESS_STATUSES } from "../../entities";
import { ILocaWebProvider } from "../../providers/ILocaWebProvider";
import { IGloboIdApiProvider } from "../../providers/IGloboIdApiProvider";
import { format, add, formatDistanceToNowStrict } from "date-fns";
import { ILogRepository } from "../../repositories/ILogRepository";

export class ReactivateUsersSalesForceUseCase {
    constructor(
        private accountRepository: IAccountRepository,
        private logRepository: ILogRepository,
        private locawebProvider: ILocaWebProvider,
        private globoIdApiProvider: IGloboIdApiProvider
    ) { }

    async execute(data: IReactivateUsersSalesForceDTO) {
        const { globoids } = data;

        let response = {};
        await mapSeries(globoids, async globo => {
            const { globoId, serviceId } = globo;

            if (!globoId || globoId === '') {
                throw new errors.BadRequest('GloboId badly formatted');
            }

            const account = await this.accountRepository.findByGloboId(globoId);
            if (account.length == 0) {
                throw new errors.NotFound('User(s) not found');
            }

            const accountFilter = account.find(acc => acc.serviceId == serviceId);
            if (!accountFilter) {
                throw new errors.NotFound('Service id not found');
            }

            if (accountFilter.opt == 'in') {
                throw new errors.BadRequest('User not opt out');
            }

            if (accountFilter.processStatus == PROCCESS_STATUSES.REACTIVATED) {
                throw new errors.Forbidden('User is already reactivated');
            }

            // Altera os status e define até quando estara reativado de acordo com a data final
            accountFilter.processStatus = PROCCESS_STATUSES.REACTIVATED;
            accountFilter.action = 3;
            accountFilter.reactivated = true;
            const finalDate = '2021-12-31';
            const finalDateToNow = formatDistanceToNowStrict(new Date(finalDate));
            var [range, type] = finalDateToNow.split(' ');
            if (parseInt(range) <= 30 && type == 'days' || type == 'day' ) {
                accountFilter.reactivatedUntil = format(add(
                    new Date(),
                    { days: parseInt(range) }
                ), 'yyyy-MM-dd');
                accountFilter.reactivatedUntilDate = add(
                    new Date(),
                    { days: parseInt(range) }
                );
            } else if(type == 'hours' || type == 'hour' ||
            type == 'minutes' || type == 'minute' ){
                accountFilter.reactivatedUntil = format(add(
                    new Date(),
                    { days: 0 }
                ), 'yyyy-MM-dd');
                accountFilter.reactivatedUntilDate = add(
                    new Date(),
                    { days: 0 }
                );
            }else if(parseInt(range) > 30 && type == 'days' || type == 'day'){
                accountFilter.reactivatedUntil = format(add(
                    new Date(),
                    { days: 31 }    
                ), 'yyyy-MM-dd');
                accountFilter.reactivatedUntilDate = add(
                    new Date(),
                    { days: 31 }
                );
            }

            // Reativa serviços e mailbox
            await this.globoIdApiProvider.insertService(accountFilter.globoId, accountFilter.serviceId);
            const [prefix, domain] = accountFilter.email.split('@');
            await this.locawebProvider.updateLocaweb(
                accountFilter.serviceId == 1 ? true : false,
                prefix,
                true
            );

            await this.accountRepository.update(accountFilter.id, accountFilter);
            response = {
                globoId: accountFilter.globoId,
                action: accountFilter.action,
                reactivatedUntil: accountFilter.reactivatedUntil
            }
        });

        return response;
    }
}