import { ReactivateUsersSalesForceController } from './ReactivateUsersSalesForceController';
import { ReactivateUsersSalesForceUseCase } from './ReactivateUsersSalesForceUseCase';
import { GloboIdApiProvider, LocaWebProvider } from '../../providers/implementation';
import { AccountRepository, LogRepository } from '../../repositories/implementation';

const accountRepository = new AccountRepository();
const logRepository = new LogRepository();
const globoIdApiProvider = new GloboIdApiProvider();
const locawebProvider = new LocaWebProvider();
const reactivateUsersSalesForceUseCase = new ReactivateUsersSalesForceUseCase(
    accountRepository,
    logRepository,
    locawebProvider,
    globoIdApiProvider
);
const reactivateUsersSalesForceController = new ReactivateUsersSalesForceController(reactivateUsersSalesForceUseCase);

export { reactivateUsersSalesForceController }