export interface IRefuseNewAccount {
    accountId: string;
    globoId: string;
}