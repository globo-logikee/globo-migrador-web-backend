import { Request, Response } from "express";
import { RefuseNewAccountUseCase } from "./RefuseNewAccountUseCase";

export class RefuseNewAccountController {
    constructor(
        private refuseNewAccountUseCase: RefuseNewAccountUseCase
    ) { }

    async handle(req: Request, res: Response): Promise<Response> {
        const { accountId, globoId } = req.body;

        try {
            const response = await this.refuseNewAccountUseCase.execute({
                accountId,
                globoId
            });

            return res.json(response);
        } catch (error) {
            const { status, message } = error;

            return res.status(status ? status : 500).json({
                statusCode: status,
                message: message
            });
        }
    }
}