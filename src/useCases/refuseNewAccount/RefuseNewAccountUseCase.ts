import { IAccountRepository } from "../../repositories/IAccountRepository";
import { IUserRepository } from "../../repositories/IUserRepository";
import { IRefuseNewAccount } from "./IRefuseNewAccount";

import errors from 'http-errors';
import { PROCCESS_STATUSES } from "../../entities/Account";
import { ILogRepository } from "../../repositories/ILogRepository";
import { Log, PROCCESS_MESSAGES } from "../../entities";
import format from "date-fns/format";
import { SmartInterventionKeys } from "../../providers/implementation";
import { IGloboIdApiProvider } from "../../providers/IGloboIdApiProvider";
import { ISalvesForceProvider } from "../../providers/ISalesForceProvider";
import { ParseAccountToSalesForceGet } from "../../utils/parseAccountToSalesforce";
import { ILocaWebProvider } from "../../providers/ILocaWebProvider";
import { actionEnum } from "../../utils/createBannerPayloLocaweb";

export class RefuseNewAccountUseCase {
    constructor(
        private userRepository: IUserRepository,
        private accountRepository: IAccountRepository,
        private logRepository: ILogRepository,
        private globoIdApiProvider: IGloboIdApiProvider,
        private salesForceProvider: ISalvesForceProvider,
        private locawebProvider: ILocaWebProvider
    ) { }

    async execute(data: IRefuseNewAccount) {
        const { accountId, globoId } = data;

        const user = await this.userRepository.finbByGloboId(globoId);
        if (!user) {
            throw new errors.NotFound('user_not_found');
        }

        const userAccount = await this.accountRepository.findById(accountId);
        if (!userAccount) {
            throw new errors.NotFound('useraccount_not_found');
        }

        // if (userAccount.processStatus !== PROCCESS_STATUSES.WAITING_USER_CONFIRM) {
        //     throw new errors.Unauthorized('unauthorized');
        // }

        userAccount.processStatus = PROCCESS_STATUSES.WAITING_OPT_OUT;
        userAccount.opt = 'out';
        userAccount.status_opt = 2;
        userAccount.decisionDate = format(new Date(), 'yyyy-MM-dd HH:mm:ss');
        await this.accountRepository.update(userAccount.id, userAccount);

        await this.logRepository.save(new Log({
            globoId,
            username: user.username,
            serviceId: userAccount.serviceId,
            userIp: userAccount.userIp ? userAccount.userIp : null,
            message: PROCCESS_MESSAGES.OPT_OUT
        }));

        const {
            smart_intervention_globomail_pro,
            smart_intervention_globomail_free
        } = SmartInterventionKeys;
        await this.globoIdApiProvider.removeSmartIntervention(
            userAccount.serviceId == 1 ? smart_intervention_globomail_pro : smart_intervention_globomail_free,
            userAccount.globoId
        );

        await this.salesForceProvider.sendToSalesForce(
            ParseAccountToSalesForceGet(userAccount)
        );

        await this.locawebProvider.sendBannerLocaweb(userAccount, actionEnum["opt-out"]);

        delete userAccount.lote;
        delete userAccount.docType;
        delete userAccount.address;
        delete userAccount.bannerSended;

        return userAccount;
    }
}