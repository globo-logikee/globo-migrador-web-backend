import { RefuseNewAccountController } from './RefuseNewAccountController';
import { RefuseNewAccountUseCase } from './RefuseNewAccountUseCase';
import { GloboIdApiProvider, LocaWebProvider, SalesForceProvider } from '../../providers/implementation';
import { UserRepository, AccountRepository, LogRepository } from '../../repositories/implementation';

const globoIdApiProvider = new GloboIdApiProvider();
const logRepository = new LogRepository();
const userRepository = new UserRepository();
const accountRepository = new AccountRepository();
const salesForceProvider = new SalesForceProvider();
const locawebProvider = new LocaWebProvider();
const refuseNewAccountuseCase = new RefuseNewAccountUseCase(userRepository, accountRepository, logRepository, globoIdApiProvider, salesForceProvider, locawebProvider);
const refuseNewAccountController = new RefuseNewAccountController(refuseNewAccountuseCase);

export { refuseNewAccountController };
