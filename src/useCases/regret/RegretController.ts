import { Request, Response } from "express";
import { RegretUseCase } from "./RegretUseCase";

export class RegretController {
    constructor(
        private regretUseCase: RegretUseCase
    ) { }

    async handle(req: Request, res: Response): Promise<Response> {
        const { accountId, globoId } = req.body;

        try {
            const response = await this.regretUseCase.execute({
                accountId,
                globoId
            });

            return res.json(response);
        } catch (error) {
            const { status, message } = error;

            return res.status(status ? status : 500).json({
                statusCode: status,
                message: message
            });
        }
    }
}