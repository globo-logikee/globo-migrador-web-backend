import { IAccountRepository } from "../../repositories/IAccountRepository";
import { ILogRepository } from "../../repositories/ILogRepository";
import { IUserRepository } from "../../repositories/IUserRepository";
import { IRegretDTO } from "./IRegretDTO";

import errors from 'http-errors';
import { PROCCESS_STATUSES } from "../../entities";

export class RegretUseCase {
    constructor(
        private userRepository: IUserRepository,
        private accountRepository: IAccountRepository,
        private logRepository: ILogRepository
    ) { }

    async execute(data: IRegretDTO) {
        const { accountId, globoId } = data;
        
        // const user = await this.userRepository.finbByGloboId(globoId);
        // if (!user) {
        //     throw new errors.NotFound('user_not_found');
        // }

        const userAccount = await this.accountRepository.findById(accountId);
        if (!userAccount) {
            throw new errors.NotFound('useraccount_not_found');
        }

        delete userAccount.lote;
        delete userAccount.docType;
        delete userAccount.address;
        delete userAccount.bannerSended;

        // userAccount.processStatus = userAccount.processStatus === 'WAITING_OPT_IN' ?
        //     PROCCESS_STATUSES.REGRET_OPT_IN :
        //     PROCCESS_STATUSES.REGRET_OPT_OUT;
        // await this.accountRepository.update(userAccount.id, userAccount);
        // delete userAccount.password;

        return userAccount;
    }
}