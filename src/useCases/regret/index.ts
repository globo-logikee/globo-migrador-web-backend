import { RegretController } from './RegretController';
import { RegretUseCase } from './RegretUseCase';
import { GloboIdApiProvider } from '../../providers/implementation';
import { UserRepository, LogRepository, AccountRepository } from '../../repositories/implementation';

const globoIdApiProvider = new GloboIdApiProvider();
const userRepository = new UserRepository();
const logRepository = new LogRepository();
const accountRepository = new AccountRepository();
const regretUseCase = new RegretUseCase(userRepository, accountRepository, logRepository);
const regretController = new RegretController(regretUseCase);

export { regretController }