export interface IRegretUserSalesForceDTO {
    globoIds: {
        globoId: string,
        resetDecision: boolean,
        serviceId: number
    }[]
}