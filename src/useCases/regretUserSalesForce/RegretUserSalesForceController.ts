import { Request, Response } from "express";
import { RegretUserSalesForceUseCase } from "./RegretUserSalesForceUseCase";

export class RegretUserSalesForceController {
    constructor(
        private regretUserSalesForce: RegretUserSalesForceUseCase
    ) { }

    async handle(req: Request, res: Response): Promise<Response> {
        const { globoIds } = req.body;

        try {
            const response = await this.regretUserSalesForce.execute({
                globoIds
            });

            return res.json(response);
        } catch (error) {
            console.log(error)
            const { status, message } = error;

            return res.status(status ? status : 500).json({
                code: status,
                reason: message
            });
        }
    }
}