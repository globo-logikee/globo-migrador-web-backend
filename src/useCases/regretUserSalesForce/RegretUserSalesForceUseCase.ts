import { mapSeries } from "p-iteration";
import { IRegretUserSalesForceDTO } from "./IRegretUserSalesForceDTO";
import { IAccountRepository } from "../../repositories/IAccountRepository";

import errors from 'http-errors';
import format from "date-fns/format";
import { Log, PROCCESS_MESSAGES, PROCCESS_STATUSES } from "../../entities";
import { add, formatDistance, formatDistanceToNowStrict, isAfter, isBefore } from "date-fns";
import { ILogRepository } from "../../repositories/ILogRepository";
import { IGloboIdApiProvider } from "../../providers/IGloboIdApiProvider";
import { ILocaWebProvider } from "../../providers/ILocaWebProvider";
import { SmartInterventionKeys, SmartInterventionValues } from "../../providers/implementation";
import { actionEnum } from "../../utils/createBannerPayloLocaweb";

export class RegretUserSalesForceUseCase {
    constructor(
        private accountRepository: IAccountRepository,
        private logRepository: ILogRepository,
        private globoIdApiProvider: IGloboIdApiProvider,
        private locawebProvider: ILocaWebProvider
    ) { }

    async execute(data: IRegretUserSalesForceDTO) {
        const { globoIds } = data;

        let response = {};
        await mapSeries(globoIds, async globoid => {
            const { globoId, resetDecision, serviceId } = globoid;

            if (!globoId || globoId === '') {
                throw new errors.BadRequest('invalid_globoid');
            }

            if (!resetDecision) {
                throw new errors.BadRequest('invalid_resetDecision');
            }

            const accounts = await this.accountRepository.findByGloboId(globoId);
            if (accounts.length == 0) {
                throw new errors.NotFound('User(s) not found');
            }

            const accountFilter = accounts.find(acc => acc.serviceId == serviceId);
            if (!accountFilter) {
                throw new errors.NotFound('ServiceId(s) not found');
            }

            if (accountFilter.processStatus == PROCCESS_STATUSES.OPT_IN_DONE ||
                accountFilter.processStatus == PROCCESS_STATUSES.OPT_OUT_DONE
                // isBefore(new Date(), new Date(accountFilter.scheduledTo))
            ) {
                throw new errors.Forbidden('User already migrated');
            }

            accountFilter.processStatus = PROCCESS_STATUSES.CREATED;
            accountFilter.status_opt = 3;
            accountFilter.action = null;
            accountFilter.opt = null;
            accountFilter.decisionDate = null;
            accountFilter.newEmail = null;
            accountFilter.password = null;
            accountFilter.changedDecision = true;
            accountFilter.changedDecisionDate = format(new Date(), 'yyyy-MM-dd HH:mm:ss');

            const isExpired = isAfter(
                new Date(),
                new Date(accountFilter.scheduledTo)
            );
            if (isExpired) {
                accountFilter.effectiveActionDate = null;

                // Verifica quantos dias faltam para a data final e calcula
                const finalDate = '2021-12-31';
                const finalDateToNow = formatDistanceToNowStrict(new Date(finalDate));
                var [range, type] = finalDateToNow.split(' ');
                if (parseInt(range) <= 30 && type == 'days' || type == 'day') {
                    accountFilter.scheduledTo = format(add(
                        new Date(),
                        { days: parseInt(range) }
                    ), 'yyyy-MM-dd');
                    accountFilter.migrationDate = add(
                        new Date(),
                        { days: parseInt(range) }
                    );
                } else if(type == 'hours' || type == 'hour' ||
                type == 'minutes' || type == 'minute' ){
                    accountFilter.scheduledTo = format(add(
                        new Date(),
                        { days: 0 }
                    ), 'yyyy-MM-dd');
                    accountFilter.migrationDate = add(
                        new Date(),
                        { days: 0 }
                    );
                }else if(parseInt(range) > 30 && type == 'days' || type == 'day') {
                    accountFilter.scheduledTo = format(add(
                        new Date(),
                        { days: 31 }
                    ), 'yyyy-MM-dd');
                    accountFilter.migrationDate = add(
                        new Date(),
                        { days: 31 }
                    );
                }

                // Reativa caixa e serviço
                const [prefix, domain] = accountFilter.email.split('@');
                await this.locawebProvider.updateLocaweb(
                    accountFilter.serviceId == 1 ? true : false,
                    prefix,
                    true
                );
                await this.globoIdApiProvider.insertService(accountFilter.globoId, accountFilter.serviceId);
            }

            await this.accountRepository.update(accountFilter.id, accountFilter);
            await this.logRepository.save(new Log({
                globoId: accountFilter.globoId,
                username: accountFilter.username,
                serviceId: accountFilter.serviceId,
                userIp: accountFilter.userIp ? accountFilter.userIp : null,
                message: PROCCESS_MESSAGES.REGRET(accountFilter.opt)
            }));

            const {
                smart_intervention_globomail_free,
                smart_intervention_globomail_pro
            } = SmartInterventionKeys;
            const {
                descontinuacao_globomail_free,
                descontinuacao_globomail_pro
            } = SmartInterventionValues;
            await this.globoIdApiProvider.insertSmartIntervention(
                accountFilter.serviceId == 1 ? smart_intervention_globomail_pro : smart_intervention_globomail_free,
                accountFilter.serviceId == 1 ? descontinuacao_globomail_pro : descontinuacao_globomail_free,
                accountFilter.globoId
            );

            await this.locawebProvider.sendBannerLocaweb(accountFilter, actionEnum.communication);

            response = {
                globoId: accountFilter.globoId,
                scheduledTo: accountFilter.scheduledTo
            }
        });

        return response;
    }
}