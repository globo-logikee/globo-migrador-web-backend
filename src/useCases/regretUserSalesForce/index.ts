import { RegretUserSalesForceController } from './RegretUserSalesForceController';
import { RegretUserSalesForceUseCase } from './RegretUserSalesForceUseCase';
import { AccountRepository, LogRepository } from '../../repositories/implementation';
import { LocaWebProvider, GloboIdApiProvider } from '../../providers/implementation';

const locawebProvider = new LocaWebProvider();
const globoIdApiProvider = new GloboIdApiProvider();
const logRepository = new LogRepository()
const accountRepository = new AccountRepository();
const regretUsersalesForceUseCase = new RegretUserSalesForceUseCase(accountRepository, logRepository, globoIdApiProvider, locawebProvider);
const regretUserSalesForceController = new RegretUserSalesForceController(regretUsersalesForceUseCase);

export { regretUserSalesForceController };