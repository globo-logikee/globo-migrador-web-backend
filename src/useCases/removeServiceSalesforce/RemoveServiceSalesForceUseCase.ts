import { mapSeries } from 'p-iteration';
import { IGloboIdApiProvider } from '../../providers/IGloboIdApiProvider';
import { ILogRepository } from '../../repositories/ILogRepository';
import { Log, PROCCESS_MESSAGES } from '../../entities';
import { IRemoveServiceSalesForce } from './IRemoveServiceSalesForceDTO';
import format from 'date-fns/format';

export class RemoveServiceSalesForceUseCase {
    constructor(
        private globoIdApiProvider: IGloboIdApiProvider,
        private logRepository: ILogRepository
    ) { }

    async execute(data: IRemoveServiceSalesForce) {
        const { globoIds } = data;

        let errResponse: { globoId: string, status: number, message: string }[] = [];

        console.log(format(new Date(), 'HH:mm:ss'), 'Iniciando processamento dos globoIds');

        await mapSeries(globoIds, async data => {
            const { globoId, serviceId } = data;

            console.log(format(new Date(), 'HH:mm:ss'), `Iniciando processamento do globoId --> ${globoId}/${serviceId} <--`);
            try {

                console.log(format(new Date(), 'HH:mm:ss'), `Buscando e deletando serviço do globoId --> ${globoId}/${serviceId} <--`);
                await this.globoIdApiProvider.removeService(globoId, parseInt(serviceId));
                console.log(format(new Date(), 'HH:mm:ss'), `Usuário encontrado e serviço deletado do globoId --> ${globoId}/${serviceId} <--`);

                await this.logRepository.save(new Log({
                    globoId,
                    serviceId: parseInt(serviceId),
                    username: '',
                    message: PROCCESS_MESSAGES.SERVICE_REMOVED(globoId, parseInt(serviceId))
                }));

            } catch (error) {
                if (error.response.status == 404) {
                    console.log(format(new Date(), 'HH:mm:ss'), `GloboId --> ${globoId}/${serviceId} <-- não encontrado`);

                    errResponse.push({
                        globoId,
                        status: error.response.status,
                        message: error.message
                    });

                    await this.logRepository.save(new Log({
                        globoId,
                        serviceId: parseInt(serviceId),
                        username: '',
                        message: PROCCESS_MESSAGES.ERR_REMOVE_SERVICE_SALESFORCE_NOT_FOUND(globoId)
                    }));
                } else {
                    console.log(format(new Date(), 'HH:mm:ss'), `Err 500 ao buscar e deletar globoId ${globoId}/${serviceId}`);

                    errResponse.push({
                        globoId,
                        status: error.response.status,
                        message: error.message
                    });

                    await this.logRepository.save(new Log({
                        globoId,
                        serviceId: parseInt(serviceId),
                        username: '',
                        message: PROCCESS_MESSAGES.ERR_REMOVE_SERVICE_SALESFORCE_INTERNAL_SERVER(globoId)
                    }));
                }

            }

            console.log(format(new Date(), 'HH:mm:ss'), `Processamento do globoId --> ${globoId}/${serviceId} <-- finalizado`);
        });

        console.log(format(new Date(), 'HH:mm:ss'), 'Processamento dos globoIds finalizado');

        if (errResponse.length != 0) {
            return { globoIds: errResponse };
        }
    }
}