import { Request, Response } from "express";
import { RemoveServiceSalesForceUseCase } from "./RemoveServiceSalesForceUseCase";

export class RemoveServiceSalesForceController {
    constructor(
        private removeServiceSalesForceUseCase: RemoveServiceSalesForceUseCase
    ) { }

    async handle(req: Request, res: Response): Promise<Response> {
        const { globoIds } = req.body;

        try {
            const response = await this.removeServiceSalesForceUseCase.execute({
                globoIds
            });

            return res.json(response);
        } catch (error) {
            console.log(error.message);
            const { status, message } = error;

            return res.status(status ? status : 500).json({
                statusCode: status,
                message: message
            });
        }
    }
}