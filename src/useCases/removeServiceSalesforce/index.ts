import { RemoveServiceSalesForceUseCase } from './RemoveServiceSalesForceUseCase';
import { RemoveServiceSalesForceController } from './RemoveServiceSalesforceCotroller';
import { GloboIdApiProvider } from '../../providers/implementation';
import { LogRepository } from '../../repositories/implementation';
 
const globoIdApiProvider = new GloboIdApiProvider();
const logRepository = new LogRepository()
const removeServiceSalesForceUseCase = new RemoveServiceSalesForceUseCase(globoIdApiProvider, logRepository);
const removeServiceSalesForceController = new RemoveServiceSalesForceController(removeServiceSalesForceUseCase);

export { removeServiceSalesForceController };