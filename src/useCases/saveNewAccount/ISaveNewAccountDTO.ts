export interface ISaveNewAccountDTO {
    accountId: string;
    globoId: string;
    password: string;
    cpf?: string;
    phone?: string;
}