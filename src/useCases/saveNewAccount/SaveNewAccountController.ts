import { Request, Response } from "express";
import { SaveNewAccountUseCase } from "./SaveNewAccountUseCase";

export class SaveNewAccountController {
    constructor(
        private saveNewAccountUseCase: SaveNewAccountUseCase
    ) { }

    async handle(req: Request, res: Response): Promise<Response> {
        const { accountId, globoId, password, phone, cpf } = req.body;

        try {
            const response = await this.saveNewAccountUseCase.execute({
                accountId,
                globoId,
                password,
                cpf,
                phone
            });

            return res.json(response);
        } catch (error) {
            console.log(error.message);
            const { status, message } = error;

            return res.status(status ? status : 500).json({
                statusCode: status,
                message: message
            });
        }
    }
}