import { IAccountRepository } from "../../repositories/IAccountRepository";
import { IUserRepository } from "../../repositories/IUserRepository";
import { ISaveNewAccountDTO } from "./ISaveNewAccountDTO";

import errors from 'http-errors';
import { IHasherProvider } from "../../providers/IHasherProvider";
import { Log, PROCCESS_MESSAGES, PROCCESS_STATUSES } from "../../entities";
import { ILogRepository } from "../../repositories/ILogRepository";
import { format } from "date-fns";
import { IGloboIdApiProvider } from "../../providers/IGloboIdApiProvider";
import { SmartInterventionKeys } from "../../providers/implementation";
import { ISalvesForceProvider } from "../../providers/ISalesForceProvider";
import { ParseAccountToSalesForce } from "../../utils/parseAccountToSalesforce";
import { ILocaWebProvider } from "../../providers/ILocaWebProvider";
import { actionEnum } from "../../utils/createBannerPayloLocaweb";

export class SaveNewAccountUseCase {
    constructor(
        private userRepository: IUserRepository,
        private accountRepository: IAccountRepository,
        private logRepository: ILogRepository,
        private hasherProvider: IHasherProvider,
        private globoIdApiProvider: IGloboIdApiProvider,
        private salesForceProvider: ISalvesForceProvider,
        private locawebProvider: ILocaWebProvider
    ) { }

    async execute(data: ISaveNewAccountDTO) {
        const { accountId, globoId, password, phone, cpf } = data;

        const user = await this.userRepository.finbByGloboId(globoId);
        if (!user) {
            throw new errors.NotFound('user_not_found');
        }

        const userAccount = await this.accountRepository.findById(accountId);
        if (!userAccount) {
            throw new errors.NotFound('useraccount_not_found');
        }

        // if (userAccount.processStatus !== PROCCESS_STATUSES.WAITING_USER_CONFIRM) {
        //     throw new errors.Unauthorized('unauthorized');
        // 

        // --> Alterar status e salva nova senha da conta
        userAccount.processStatus = PROCCESS_STATUSES.WAITING_OPT_IN;
        userAccount.opt = 'in';
        userAccount.status_opt = 1;
        userAccount.decisionDate = format(new Date(), 'yyyy-MM-dd HH:mm:ss');
        userAccount.length = password.length;
        userAccount.password = await this.hasherProvider.encrypt(password);

        if (!userAccount.phones || !userAccount.phones.phone && phone) {
            const dddPhone = phone.substr(0, 2);
            const phoneNumber = phone.substr(2, phone.length);
            userAccount.phones = {
                phone: await this.hasherProvider.encrypt(phoneNumber),
                dddPhone
            }
        }
        if (!userAccount.docNumber && cpf) {
            userAccount.docNumber = await this.hasherProvider.encrypt(cpf);
        }
        await this.accountRepository.update(userAccount.id, userAccount);
        delete userAccount.password;

        await this.logRepository.save(new Log({
            globoId,
            username: user.username,
            userIp: userAccount.userIp ? userAccount.userIp : null,
            message: PROCCESS_MESSAGES.NEW_PASSWORD
        }));

        await this.logRepository.save(new Log({
            globoId,
            username: user.username,
            userIp: userAccount.userIp ? userAccount.userIp : null,
            message: PROCCESS_MESSAGES.OPT_IN
        }));

        const {
            smart_intervention_globomail_pro,
            smart_intervention_globomail_free
        } = SmartInterventionKeys;
        await this.globoIdApiProvider.removeSmartIntervention(
            userAccount.serviceId == 1 ? smart_intervention_globomail_pro : smart_intervention_globomail_free,
            userAccount.globoId
        );

        await this.salesForceProvider.sendToSalesForce(
            ParseAccountToSalesForce(userAccount)
        );

        await this.locawebProvider.sendBannerLocaweb(userAccount, actionEnum["opt-in"]);

        delete userAccount.lote;
        delete userAccount.docType;
        delete userAccount.address;
        delete userAccount.bannerSended;
        delete userAccount.password;

        return userAccount;
    }
}