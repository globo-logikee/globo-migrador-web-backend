import { SaveNewAccountController } from './SaveNewAccountController';
import { SaveNewAccountUseCase } from './SaveNewAccountUseCase';
import { BcryptProvider, GloboIdApiProvider, LocaWebProvider, SalesForceProvider } from '../../providers/implementation';
import { UserRepository, AccountRepository, LogRepository } from '../../repositories/implementation';

const globoIdApiProvider = new GloboIdApiProvider();
const logRepository = new LogRepository()
const bcryptProvider = new BcryptProvider();
const userRepository = new UserRepository();
const accountRepository = new AccountRepository();
const salesForceProvider = new SalesForceProvider();
const locawebProvider = new LocaWebProvider();
const saveNewAccountUseCase = new SaveNewAccountUseCase(userRepository, accountRepository, logRepository, bcryptProvider, globoIdApiProvider, salesForceProvider, locawebProvider);
const saveNewAccountController = new SaveNewAccountController(saveNewAccountUseCase);

export { saveNewAccountController };