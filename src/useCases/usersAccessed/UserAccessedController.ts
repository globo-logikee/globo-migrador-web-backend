import { Request, Response } from "express";
import { UserAccessedUseCase } from "./UserAccessedUseCase";

export class UserAccessedController {
    constructor(
        private userAccessedUseCase: UserAccessedUseCase
    ) { }

    async handle(req: Request, res: Response): Promise<Response> {
        try {
            const response = await this.userAccessedUseCase.execute();

            return res.json(response);
        } catch (error) {
            console.log(error.message);
            const { status, message } = error;

            return res.status(status ? status : 500).json({
                statusCode: status,
                message: message
            });
        }
    }
}