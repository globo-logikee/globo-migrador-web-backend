import { format } from "date-fns";
import { mapSeries } from "p-iteration";
import { PROCCESS_MESSAGES, User } from "../../entities";
import { ILogRepository } from "../../repositories/ILogRepository";
import { IUserRepository } from "../../repositories/IUserRepository";

export class UserAccessedUseCase {
    constructor(
        private userRepository: IUserRepository,
        private logRepository: ILogRepository
    ) { }

    async execute() {
        var usersWithLogin = await this.userRepository.countUsers({
            accessed: true
        });

        return {
            appointment_date_time: `${format(new Date(), 'yyyy-MM-dd HH:mm:ss')}`,
            total_users: usersWithLogin
        };
    }
}