import { UserAccessedController } from './UserAccessedController';
import { UserAccessedUseCase } from './UserAccessedUseCase';
import { UserRepository, LogRepository } from '../../repositories/implementation';

const logRepository = new LogRepository();
const userRepository = new UserRepository();
const userAccessedUseCase = new UserAccessedUseCase(userRepository, logRepository);
const userAccessedController = new UserAccessedController(userAccessedUseCase);

export { userAccessedController };