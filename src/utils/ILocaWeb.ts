export interface ILocaWebCreateModel {
    id_globo: string;
    person_type: string;
    current_email_address: string;
    alias_email_address?: string;
    password: string;
    name: string;
    company_name?: string;
    cpf: string;
    cnpj?: number;
    rg?: string;
    phones: { number: string }[];
    emails: { address: string, main: boolean, confirmed: boolean }[];
    address?: {
        city: string,
        state: string,
        postal_code: string,
        country: string,
        number: string,
        street: string
    }
}

export interface IBannerLocaWeb {
    id_globo: string;
    current_email_address: string;
    message: string;
    background_color: string;
    message_link: string;
    redirect_link: string;
    titulo_alert: string;
    message_alert: string;
    message_link_alert: string;
    redirect_link_alert: string;
}