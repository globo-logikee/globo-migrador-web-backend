import { add, format, sub } from "date-fns";
import { Account } from "../entities";
import { IBannerLocaWeb } from "./ILocaWeb";

export enum actionEnum {
    'communication',
    'opt-in',
    'opt-out'
}

function parseScheduleTo(scheduledTo: string, decrement: boolean) {
    console.log(scheduledTo);
    if (decrement) {
        return format(sub(new Date(scheduledTo), { days: 0 }), 'dd/MM/yyyy');
    }

    return format(add(new Date(scheduledTo), { days: 1 }), 'dd/MM/yyyy');
}

export function CreateBannerPayloadLocaweb(account: Account, action: actionEnum): IBannerLocaWeb {
    let payload: IBannerLocaWeb = {
        id_globo: '',
        current_email_address: '',
        message: '',
        background_color: '',
        message_link: '',
        redirect_link: '',
        titulo_alert: '',
        message_alert: '',
        message_link_alert: '',
        redirect_link_alert: ''
    }

    if (action == actionEnum.communication) {
        payload = {
            id_globo: `${account.globoId}/${account.serviceId}`,
            current_email_address: account.email,
            message: `${account.fullName.split(' ')[0]}, seu Globomail será descontinuado em breve. Para não perder suas mensagens, escolha o que deseja fazer até ${parseScheduleTo(account.scheduledTo, true)}.`,
            background_color: '#CA2A35',
            message_link: 'Escolher agora',
            redirect_link: 'https://descontinuacaoglobomail.globo.com/',
            titulo_alert: 'Seu Globomail será descontinuado em breve',
            message_alert: `${account.fullName.split(' ')[0]}, para não perder suas mensagens, é importante escolher o que deseja fazer com sua caixa de e-mail até ${parseScheduleTo(account.scheduledTo, true)}.`,
            message_link_alert: 'Escolher agora',
            redirect_link_alert: 'https://descontinuacaoglobomail.globo.com/'
        }
    } else if (action == actionEnum["opt-in"]) {
        payload = {
            id_globo: `${account.globoId}/${account.serviceId}`,
            current_email_address: account.email,
            message: `Seu Globomail será descontinuado em breve. A partir de ${parseScheduleTo(account.scheduledTo, false)}, seu e-mail muda para ${account.newEmail} e passa a ser de responsabilidade da Locaweb.`,
            background_color: '#F49D36',
            message_link: 'Saiba mais sobre a descontinuação',
            redirect_link: 'https://ajuda.globo/globoplay/web/globomail/descontinuacao-globomail/',
            titulo_alert: 'Você escolheu mudar para a Locaweb',
            message_alert: `Lembre-se que o seu Globomail será descontinuado em breve. A partir de ${parseScheduleTo(account.scheduledTo, false)}, seu e-mail muda para ${account.newEmail} e passa a ser de responsabilidade da Locaweb.`,
            message_link_alert: 'Saiba mais',
            redirect_link_alert: 'https://ajuda.globo/globoplay/web/globomail/descontinuacao-globomail/'
        }
    } else if (action == actionEnum["opt-out"]) {
        payload = {
            id_globo: `${account.globoId}/${account.serviceId}`,
            current_email_address: account.email,
            message: `Seu Globomail será descontinuado em breve. Como você escolheu não mudar para Locaweb, lembre-se de transferir suas mensagens para um e-mail de sua preferência até ${parseScheduleTo(account.scheduledTo, true)}.`,
            background_color: '#F49D36',
            message_link: 'Veja como transferir suas mensagens',
            redirect_link: 'https://ajuda.globo/globoplay/web/globomail/descontinuacao-globomail/faq/como-transferir-minhas-mensagens-do-globomail-para-outro-e-mail.ghtml',
            titulo_alert: 'Transfira suas mensagens para outro e-mail',
            message_alert: `Seu Globomail será descontinuado em breve. Como você escolheu não mudar para Locaweb, lembre-se de transferir suas mensagens para um e-mail de sua preferência até ${parseScheduleTo(account.scheduledTo, true)}.`,
            message_link_alert: 'Saiba mais',
            redirect_link_alert: 'https://ajuda.globo/globoplay/web/globomail/descontinuacao-globomail/faq/como-transferir-minhas-mensagens-do-globomail-para-outro-e-mail.ghtml'
        }
    }

    return payload;
}