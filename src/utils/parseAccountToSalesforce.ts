import { Account } from "../entities";

export interface IUsersSalesForceResponseDTO {
    globoId: string;
    email?: string;
    username?: string;
    status_opt?: number;
    decisionDate?: string;
    firstCommunicationDate?: string;
    scheduledTo?: string;
    effectiveActionDate?: string;
    globoIdDeactivationDate?: string;
    action?: number;
    changedDecision?: string;
    changedDecisionDate?: string;
    serviceId?: number;
    lote?: string;
    erro?: {
        code: number,
        reason: string
    }
}

export function ParseAccountToSalesForceGet(account: Account) {
    const {
        globoId,
        email,
        username,
        status_opt,
        decisionDate,
        firstCommunicationDate,
        scheduledTo,
        effectiveActionDate,
        globoIdDeactivationDate,
        action,
        changedDecision,
        changedDecisionDate,
        serviceId,
        lote
    } = account;

    const salesforceUser = {
        globoId,
        email,
        username,
        status_opt,
        decisionDate: decisionDate ? decisionDate : null,
        firstCommunicationDate,
        scheduledTo,
        effectiveActionDate: effectiveActionDate ? effectiveActionDate : null,
        globoIdDeactivationDate: globoIdDeactivationDate ? globoIdDeactivationDate : null,
        action: action ? action : null,
        changedDecision: changedDecision ? changedDecision : null,
        changedDecisionDate: changedDecisionDate ? changedDecisionDate : null,
        serviceId,
        lote: lote ? lote : null,
        erro: {
            code: null,
            reason: null
        }
    };

    return salesforceUser;
}

export function ParseAccountToSalesForce(account: Account) {
    const {
        globoId,
        email,
        username,
        status_opt,
        decisionDate,
        firstCommunicationDate,
        scheduledTo,
        effectiveActionDate,
        globoIdDeactivationDate,
        action,
        changedDecision,
        changedDecisionDate,
        serviceId,
        lote
    } = account;

    const salesforceUser = {
        globoId,
        email,
        username,
        status_opt,
        decisionDate: decisionDate ? decisionDate : null,
        firstCommunicationDate,
        scheduledTo,
        effectiveActionDate: effectiveActionDate ? effectiveActionDate : null,
        globoIdDeactivationDate: globoIdDeactivationDate ? globoIdDeactivationDate : null,
        action: action ? action : null,
        serviceId,
        lote: lote ? lote : null,
    };

    return salesforceUser;
}