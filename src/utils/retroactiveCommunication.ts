import { mapSeries } from "p-iteration";
import { Account, PROCCESS_STATUSES } from "../entities";
import { createConnection, getRepository } from "typeorm";
import { actionEnum, CreateBannerPayloadLocaweb } from "./createBannerPayloLocaweb";
import { IBannerLocaWeb } from './ILocaWeb';
import { format } from "date-fns";
import { createReadStream, existsSync, mkdirSync, writeFileSync } from "fs";
import path from "path";
import { mkdir } from "fs/promises";

const writeFile = async (path, content) => {
    // mkdirSync('json');
    writeFileSync(path, JSON.stringify(content), 'utf8');
}

async function bootstrap() {
    await createConnection().then(() => {
        console.log('Script retroactive banner communication started!');
    }).catch(error => { throw error });

    const accountRepository = getRepository(Account);

    console.log('Searching accounts...');
    const accounts = await accountRepository.find({
        where: {
            $or: [{ bannerSended: null }, { bannerSended: undefined }]
        }
    });

    if (accounts.length == 0) {
        console.log('0 accounts do process...');
        return;
    }

    console.log(`Processing ${accounts.length} found accounts...`);
    let banners: IBannerLocaWeb[] = [];
    let obj = {
        banners
    }
    await mapSeries(accounts, async account => {
        let action: actionEnum;
        if (
            account.processStatus == PROCCESS_STATUSES.SALESFORCE_INPUT ||
            account.processStatus == PROCCESS_STATUSES.CREATED ||
            account.processStatus == PROCCESS_STATUSES.WAITING_USER_CONFIRM ||
            account.processStatus == PROCCESS_STATUSES.REGRET_OPT_IN ||
            account.processStatus == PROCCESS_STATUSES.REGRET_OPT_OUT
        ) {
            action = actionEnum.communication;
        } else if (
            account.processStatus == PROCCESS_STATUSES.WAITING_OPT_IN ||
            account.processStatus == PROCCESS_STATUSES.WAITING_ACCEPT_LW_IN ||
            account.processStatus == PROCCESS_STATUSES.OPT_IN_DONE
        ) {
            action = actionEnum["opt-in"];
        } else if (
            account.processStatus == PROCCESS_STATUSES.WAITING_OPT_OUT ||
            account.processStatus == PROCCESS_STATUSES.WAITING_ACCEPT_LW_OUT ||
            account.processStatus == PROCCESS_STATUSES.OPT_OUT_DONE ||
            account.processStatus == PROCCESS_STATUSES.NOT_ACCEPT_OPT_OUT_DONE
        ) {
            action = actionEnum["opt-out"];
        }


        const payload = CreateBannerPayloadLocaweb(account, action);
        banners.push(payload);

        console.log(`Globoid processed: ${account.globoId}/${account.serviceId}`);
    });

    // const filename = `banner-payload-${format(new Date(), 'yyyy-MM-dd')}`;
    // // const jsonPath = `${__dirname}/json/${path.basename(filename)}.json`;
    // console.log(__dirname);
    // await mkdir('./src/utils/json');
    // const jsonPath = path.resolve(__dirname, `./json/${filename}.json`);
    // writeFileSync(jsonPath, JSON.stringify(obj), 'utf8');
    writeFileSync('./content.json', JSON.stringify(obj));

    console.log('Banners [json] created on root directory');
}

bootstrap();