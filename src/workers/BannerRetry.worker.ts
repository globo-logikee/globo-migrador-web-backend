import { Account, PROCCESS_STATUSES } from "../entities";
import sleep from "../utils/sleep";
import { createConnection, getRepository } from "typeorm";
import { mapSeries } from "p-iteration";
import { LocaWebProvider } from "../providers/implementation";
import { actionEnum } from "../utils/createBannerPayloLocaweb";

async function bootstrap() {
    await createConnection().then(() => {
        console.log('Worker banner retry started!');
    }).catch(error => { throw error });

    const accountRepository = getRepository(Account);
    const locawebProvider = new LocaWebProvider();

    while (true) {
        const accounts = await accountRepository.find({ bannerSended: false });
        console.log(accounts);
        if (accounts.length != 0) {
            await mapSeries(accounts, async account => {
                try {
                    let action: number;
                    if (
                        account.processStatus == PROCCESS_STATUSES.SALESFORCE_INPUT ||
                        account.processStatus == PROCCESS_STATUSES.CREATED ||
                        account.processStatus == PROCCESS_STATUSES.WAITING_USER_CONFIRM ||
                        account.processStatus == PROCCESS_STATUSES.REGRET_OPT_IN ||
                        account.processStatus == PROCCESS_STATUSES.REGRET_OPT_OUT
                    ) {
                        action = actionEnum.communication
                    } else if (
                        account.processStatus == PROCCESS_STATUSES.WAITING_OPT_IN ||
                        account.processStatus == PROCCESS_STATUSES.WAITING_ACCEPT_LW_IN ||
                        account.processStatus == PROCCESS_STATUSES.OPT_IN_DONE
                    ) {
                        action = actionEnum["opt-in"]
                    } else if (
                        account.processStatus == PROCCESS_STATUSES.WAITING_OPT_OUT ||
                        account.processStatus == PROCCESS_STATUSES.WAITING_ACCEPT_LW_OUT ||
                        account.processStatus == PROCCESS_STATUSES.OPT_OUT_DONE
                    ) {
                        action = actionEnum["opt-out"]
                    }

                    await locawebProvider.sendBannerLocaweb(account, action);
                } catch (error) {
                    console.log('err worker banner retry', error.message);
                }
            });
        }

        await sleep(4000);
    }
}

bootstrap();