import { Account, PROCCESS_STATUSES, Log, PROCCESS_MESSAGES } from "../entities";
import { createConnection, getRepository } from "typeorm";
import sleep from "../utils/sleep";
import { LocaWebProvider, GloboIdApiProvider, SalesForceProvider } from "../providers/implementation";
import { format } from "date-fns";
import { mapSeries } from "p-iteration";
import { LogRepository } from '../repositories/implementation';
import { ParseAccountToSalesForce } from "../utils/parseAccountToSalesforce";

async function bootstrap() {
    await createConnection().then(() => {
        console.log('Worker globo id api disable opt in started!');
    }).catch(error => { throw error });

    const accountRepository = getRepository(Account);
    const logRepository = new LogRepository();
    const locaWebProvider = new LocaWebProvider();
    const globoIdApiProvider = new GloboIdApiProvider();
    const salesForceProvicer = new SalesForceProvider();

    while (true) {
        console.log(format(new Date(), 'HH:mm:ss'), `Etapa 1: Buscando contas WAITING_ACCEPT_LW_IN`);
        const accounts = await accountRepository.find({
            processStatus: PROCCESS_STATUSES.WAITING_ACCEPT_LW_IN,
            opt: 'in'
        });

        if (accounts.length != 0) {
            console.log(format(new Date(), 'HH:mm:ss'), `Etapa 1: ${accounts.length} contas encontradas`);

            await mapSeries(accounts, async account => {
                try {
                    console.log(format(new Date(), 'HH:mm:ss'), `Iniciando processamento do globoid ${account.globoId}`)
                    console.log(format(new Date(), 'HH:mm:ss'), `Etapa 2: Buscando status na locaweb do globoid ${account.globoId}`);
                    const responses: [{
                        status_code: number,
                        status_name: string
                    }] = await locaWebProvider.consultStatus([{
                        id_globo: `${account.globoId}/${account.serviceId}`
                    }]);
                    console.log(format(new Date(), 'HH:mm:ss'), `Etapa 2: Status encontrado na locaweb do globoid ${account.globoId}`);

                    await mapSeries(responses, async response => {
                        const { status_code, status_name } = response;

                        if (status_code == 3 && status_name == 'Concluído') {
                            console.log(format(new Date(), 'HH:mm:ss'), `Status na locaweb esta concluido do globoid ${account.globoId}`);

                            console.log(format(new Date(), 'HH:mm:ss'), `Etapa 3: Removendo serviços na globoId api do globoid ${account.globoId}`);
                            await globoIdApiProvider.removeService(account.globoId, account.serviceId);
                            await globoIdApiProvider.removeService(account.globoId, 84);
                            console.log(format(new Date(), 'HH:mm:ss'), `Etapa 3: Serviços removidos na globoId api do globoid ${account.globoId}`);

                            account.processStatus = PROCCESS_STATUSES.OPT_IN_DONE;
                            account.globoIdDeactivationDate = format(new Date(), 'yyyy-MM-dd HH:mm:ss');
                            account.action = 1;
                            account.effectiveActionDate = format(new Date(), 'yyyy-MM-dd HH:mm:ss');

                            if (account.globoMail) {
                                console.log(format(new Date(), 'HH:mm:ss'), `Conta com campo globoMail do globoid ${account.globoId}`);
                                const toUpdateGloboMail = [
                                    'globo.com',
                                    'globomail.com'
                                ];
                                if (toUpdateGloboMail.includes(account.globoMail.split('@')[1])) {
                                    console.log(format(new Date(), 'HH:mm:ss'), `Etapa 4: Atualizando email na globoId api do globoid ${account.globoId}`);
                                    await globoIdApiProvider.updateToLwMail(account.globoId, account.newEmail);
                                    console.log(format(new Date(), 'HH:mm:ss'), `Etapa 4: Email atualizado na globoId api do globoid ${account.globoId}`);
                                }
                            } else {
                                console.log(format(new Date(), 'HH:mm:ss'), `Etapa 4: Conta sem campo globoMail do globoid ${account.globoId}`);
                            }

                            console.log(format(new Date(), 'HH:mm:ss'), `Etapa 5: Atualizando status na nossa base de dados do globoid ${account.globoId}`);
                            await accountRepository.update(account.id, account);
                            console.log(format(new Date(), 'HH:mm:ss'), `Etapa 5: Status atualizado na nossa base de dados do globoid ${account.globoId}`);

                            console.log(format(new Date(), 'HH:mm:ss'), `Etapa 6: Iniciando envio para salesforce do globoid ${account.globoId}`);
                            await salesForceProvicer.sendToSalesForce(
                                ParseAccountToSalesForce(account)
                            );
                            console.log(format(new Date(), 'HH:mm:ss'), `Etapa 6: Envio para salesforce finalizado do globoid ${account.globoId}`);

                            console.log(format(new Date(), 'HH:mm:ss'), `GloboId ${account.globoId} on globoIdDisableIn worker done`);
                        } else {
                            console.log(format(new Date(), 'HH:mm:ss'), `Status na locaweb nao concluido do globoid ${account.globoId}`);
                        }
                    });
                } catch (error) {
                    console.log('err worker globoid api disable opt in', error);
                    // console.log(error.message);

                    const { response } = error;

                    if (response) {
                        if (response.status == 400) {
                            console.log(format(new Date(), 'HH:mm:ss'), 'Err 400 worker globo id api disable in', error.message);

                            await logRepository.save(new Log({
                                globoId: account.globoId,
                                username: account.username,
                                serviceId: account.serviceId,
                                userIp: account.userIp ? account.userIp : null,
                                message: PROCCESS_MESSAGES.ERR_GLOBO_ID_API_DISABLE_IN(account.globoId, error.message)
                            }));

                            if (response.config.url) {
                                const urlSplited = response.config.url.split('/');
                                if (urlSplited[urlSplited.length - 1] == account.globoId) {
                                    console.log(format(new Date(), 'HH:mm:ss'), `Erro no update email na globoidApi do globoid ${account.globoId}`);

                                    console.log(format(new Date(), 'HH:mm:ss'), `Continuando fluxo ${account.globoId}`);
                                    account.processStatus = PROCCESS_STATUSES.OPT_IN_DONE;
                                    account.globoIdDeactivationDate = format(new Date(), 'yyyy-MM-dd HH:mm:ss');
                                    account.action = 1;
                                    account.effectiveActionDate = format(new Date(), 'yyyy-MM-dd HH:mm:ss');

                                    console.log(format(new Date(), 'HH:mm:ss'), `Etapa 5: Atualizando status na nossa base de dados do globoid ${account.globoId}`);
                                    await accountRepository.update(account.id, account);
                                    console.log(format(new Date(), 'HH:mm:ss'), `Etapa 5: Atualizado na base de dados do globoid ${account.globoId}`);

                                    console.log(format(new Date(), 'HH:mm:ss'), `Etapa 6: Iniciando envio para salesforce do globoid ${account.globoId}`);
                                    await salesForceProvicer.sendToSalesForce(
                                        ParseAccountToSalesForce(account)
                                    );
                                    console.log(format(new Date(), 'HH:mm:ss'), `Etapa 6: Envio para salesforce finalizado do globoid ${account.globoId}`);

                                    console.log(format(new Date(), 'HH:mm:ss'), `GloboId ${account.globoId} on globoid disable in worker done`);
                                }
                            }
                        }

                        if (response.status == 422) {
                            await logRepository.save(new Log({
                                globoId: account.globoId,
                                username: account.username,
                                serviceId: account.serviceId,
                                userIp: account.userIp ? account.userIp : null,
                                message: PROCCESS_MESSAGES.ERR_GET_LOCAWEB('Unprocessable Entity', account.email)
                            }));
                        }
                    }
                }
            });
        }

        await sleep(60000);
    }
}

bootstrap();