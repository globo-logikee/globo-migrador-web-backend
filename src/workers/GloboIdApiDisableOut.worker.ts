import sleep from "../utils/sleep";
import { GloboIdApiProvider } from '../providers/implementation';
import { LogRepository } from '../repositories/implementation';
import { createConnection, getRepository, LessThan, LessThanOrEqual } from "typeorm";
import { Account, PROCCESS_STATUSES, Log, PROCCESS_MESSAGES } from "../entities";
import { mapSeries } from "p-iteration";
import { add, format } from "date-fns";

const LessThanDate = (date: Date) => LessThanOrEqual(format(date, 'yyyy-MM-dd HH:MM:SS'));

async function bootstrap() {
    await createConnection().then(() => {
        console.log('Worker globo id api disable opt out started!');
    }).catch(error => { throw error });

    const accountRepository = getRepository(Account);
    const logRepository = new LogRepository();
    const globoIdApiProvider = new GloboIdApiProvider();

    while (true) {
        const accounts = await accountRepository.find({
            where: {
                $or: [
                    {
                        processStatus: PROCCESS_STATUSES.WAITING_OPT_OUT,
                        opt: 'out',
                        scheduledTo: format(new Date(), 'yyyy-MM-dd'),
                    },
                    {
                        processStatus: PROCCESS_STATUSES.WAITING_OPT_OUT,
                        opt: 'out',
                        migrationDate: { $lt: new Date() }
                    }
                ]
            }
        });

        if (accounts.length != 0) {
            await mapSeries(accounts, async account => {
                try {
                    const remove = await globoIdApiProvider.removeService(account.globoId, account.serviceId);
                    await globoIdApiProvider.removeService(account.globoId, 84);
                    if (remove.status == 200) {
                        account.processStatus = PROCCESS_STATUSES.WAITING_ACCEPT_LW_OUT;
                        account.globoIdDeactivationDate = format(new Date(), 'yyyy-MM-dd HH:mm:ss');
                    }

                    await accountRepository.update(account.id, account);
                    console.log(`globoId ${account.globoId} on globoid disable out worker done`);
                } catch (error) {
                    console.log('err worker globoid api disable opt out', error);

                    const { response } = error;

                    if (response) {
                        if (response.status == 401) {
                            await logRepository.save(new Log({
                                globoId: account.globoId,
                                username: account.username,
                                serviceId: account.serviceId,
                                userIp: account.userIp ? account.userIp : null,
                                message: PROCCESS_MESSAGES.ERR_GLOBOID_API('Unauthorized')
                            }));
                        }
                    }
                }
            });
        }

        await sleep(25000);
    }
}

bootstrap();