import sleep from '../utils/sleep';
import { Account, PROCCESS_STATUSES, Log, PROCCESS_MESSAGES, phones } from '../entities';
import { createConnection, getRepository } from 'typeorm';
import { format } from 'date-fns';
import { locaWebInService } from './services/locawebIn';
import { LocaWebProvider } from '../providers/implementation';
import { mapSeries } from 'p-iteration';
import { LogRepository } from '../repositories/implementation';
import { BcryptProvider } from '../providers/implementation/BcryptProvider';

async function bootstrap() {
    await createConnection().then(() => {
        console.log('Worker local web opt in started!');
    }).catch(error => { throw error });

    const accountRepository = getRepository(Account);
    const logRepository = new LogRepository();
    const locaWebProvider = new LocaWebProvider();
    const bcryptProvider = new BcryptProvider();

    while (true) {
        const accounts = await accountRepository.find({
            where: {
                $or: [
                    {
                        processStatus: PROCCESS_STATUSES.WAITING_OPT_IN,
                        opt: 'in',
                        scheduledTo: format(new Date(), 'yyyy-MM-dd'),
                    },
                    {
                        processStatus: PROCCESS_STATUSES.WAITING_OPT_IN,
                        opt: 'in',
                        migrationDate: { $lt: new Date() }
                    }
                ]
            }
        });

        if (accounts.length != 0) {
            await mapSeries(accounts, async account => {
                try {
                    const passwordDecrypted = await bcryptProvider.decrypt(account.password);

                    let docNumberDecrypted: string;
                    if (account.docNumber) {
                        docNumberDecrypted = await bcryptProvider.decrypt(account.docNumber);
                    }

                    let phones: phones;
                    if (account.phones) {
                        let dddCellPhone;
                        let cellPhone;
                        if (account.phones.cellPhone) {
                            dddCellPhone = account.phones.dddCellPhone;
                            cellPhone = await bcryptProvider.decrypt(account.phones.cellPhone);
                        }

                        let dddPhone;
                        let phone;
                        if (account.phones.phone) {
                            dddPhone = account.phones.dddPhone;
                            phone = await bcryptProvider.decrypt(account.phones.phone);
                        }

                        phones = {
                            dddCellPhone,
                            cellPhone,
                            dddPhone,
                            phone
                        }
                    }

                    const accountFormated = locaWebInService.createModelLw(account, { password: passwordDecrypted, phones, docNumber: docNumberDecrypted });

                    const response = await locaWebProvider.createLocaweb([accountFormated]);
                    if (response.status == 202) {
                        account.processStatus = PROCCESS_STATUSES.WAITING_ACCEPT_LW_IN;
                        await accountRepository.update(account.id, account);
                    }

                    console.log(`globoId ${account.globoId} on locaweb in worker done`);
                } catch (error) {
                    console.log('err worker locaweb opt in', error);

                    const { response } = error;
                    if (response) {
                        if (response.data) {
                            if (response.data.error_code == 1062) {
                                await logRepository.save(new Log({
                                    globoId: account.globoId,
                                    username: account.username,
                                    serviceId: account.serviceId,
                                    userIp: account.userIp ? account.userIp : null,
                                    message: PROCCESS_MESSAGES.ERR_CREATE_LOCAWEB('Already exists on locaweb', account.email, account.newEmail),
                                    apiMessage: `Globo Id: ${account.globoId} Response data: ${JSON.stringify(response.data)}`
                                }));
                                account.processStatus = PROCCESS_STATUSES.ALREADY_EXISTS_LW;
                                accountRepository.update(account.id, account);
                            }
                        }

                        if (response.status) {
                            if (response.status == 400) {
                                await logRepository.save(new Log({
                                    globoId: account.globoId,
                                    username: account.username,
                                    serviceId: account.serviceId,
                                    userIp: account.userIp ? account.userIp : null,
                                    message: PROCCESS_MESSAGES.ERR_CREATE_LOCAWEB('Bad request', account.email, account.newEmail),
                                    apiMessage: `Globo Id: ${account.globoId} Response data: ${JSON.stringify(response.data)}`
                                }));
                            }
                            if (response.status == 422) {
                                await logRepository.save(new Log({
                                    globoId: account.globoId,
                                    username: account.username,
                                    serviceId: account.serviceId,
                                    userIp: account.userIp ? account.userIp : null,
                                    message: PROCCESS_MESSAGES.ERR_CREATE_LOCAWEB('Unprocessable Entity', account.email, account.newEmail),
                                    apiMessage: `Globo Id: ${account.globoId} Response data: ${JSON.stringify(response.data)}`
                                }));
                            }
                        }
                    }
                }
            });
        } else {
            console.log('no accounts to opt in');
        }

        await sleep(25000);
    }
}

bootstrap();
