import { Account, PROCCESS_STATUSES, Log, PROCCESS_MESSAGES } from "../entities";
import { LocaWebProvider, SalesForceProvider } from "../providers/implementation";
import { createConnection, getRepository } from "typeorm";
import sleep from "../utils/sleep";
import { format } from "date-fns";
import { mapSeries } from "p-iteration";
import { LogRepository } from '../repositories/implementation';
import { ParseAccountToSalesForce } from "../utils/parseAccountToSalesforce";

async function bootstrap() {
    await createConnection().then(() => {
        console.log('Worker local web opt out started!');
    }).catch(error => { throw error });

    const accountRepository = getRepository(Account);
    const logRepository = new LogRepository();
    const locaWebProvider = new LocaWebProvider();
    const salesForceProvicer = new SalesForceProvider();

    while (true) {
        console.log(format(new Date(), 'HH:mm:ss'), `Etapa 1: Buscando contas WAITING_ACCEPT_LW_OUT`);
        const accounts = await accountRepository.find({
            processStatus: PROCCESS_STATUSES.WAITING_ACCEPT_LW_OUT
        });

        if (accounts.length != 0) {
            console.log(format(new Date(), 'HH:mm:ss'), `Etapa 1: ${accounts.length} contas encontradas`);

            await mapSeries(accounts, async account => {
                try {
                    console.log(format(new Date(), 'HH:mm:ss'), `Iniciando processamento do globoId ${account.globoId}`)

                    console.log(format(new Date(), 'HH:mm:ss'), `Etapa 2: Atualizando mail box na lw do globoid ${account.globoId}`);
                    const [prefix, domain] = account.email.split('@');
                    await locaWebProvider.updateLocaweb(
                        account.serviceId == 1 ? true : false,
                        prefix,
                        false
                    );
                    console.log(format(new Date(), 'HH:mm:ss'), `Etapa 2: Mail box atualiada na lw do globoid ${account.globoId}`);

                    if (account.status_opt == 4) {
                        account.processStatus = PROCCESS_STATUSES.NOT_ACCEPT_OPT_OUT_DONE;
                    } else {
                        account.processStatus = PROCCESS_STATUSES.OPT_OUT_DONE;
                    }

                    account.action = 2;
                    account.effectiveActionDate = format(new Date(), 'yyyy-MM-dd HH:mm:ss');

                    console.log(format(new Date(), 'HH:mm:ss'), `Etapa 3: Atualizando status na nossa base de dados do globoid ${account.globoId}`);
                    await accountRepository.update(account.id, account);
                    console.log(format(new Date(), 'HH:mm:ss'), `Etapa 3: Status atualizado na nossa base de dados do globoid ${account.globoId}`)

                    console.log(format(new Date(), 'HH:mm:ss'), `Etapa 4: Enviando para salesforce globoid ${account.globoId}`);
                    await salesForceProvicer.sendToSalesForce(
                        ParseAccountToSalesForce(account)
                    );
                    console.log(format(new Date(), 'HH:mm:ss'), `Etapa 4: Enviado para salesforce globoid ${account.globoId}`);

                    console.log(format(new Date(), 'HH:mm:ss'), `Globoid ${account.globoId} on locaweb out worker done`);
                } catch (error) {
                    console.log('err worker locaweb opt out', error);

                    const { response } = error;

                    if (response) {
                        if (response.status == 404) {
                            const { data } = response;
                            if (data.domain) {
                                await logRepository.save(new Log({
                                    globoId: account.globoId,
                                    username: account.username,
                                    serviceId: account.serviceId,
                                    userIp: account.userIp ? account.userIp : null,
                                    message: PROCCESS_MESSAGES.ERR_UPDATE_LOCAWEB('Not Found', 'domain', data.domain)
                                }));
                            }
                            if (data.mailbox) {
                                await logRepository.save(new Log({
                                    globoId: account.globoId,
                                    username: account.username,
                                    serviceId: account.serviceId,
                                    userIp: account.userIp ? account.userIp : null,
                                    message: PROCCESS_MESSAGES.ERR_UPDATE_LOCAWEB('Not Found', 'mailbox', data.mailbox)
                                }));
                            }
                        }
                    }
                }
            });
        }

        await sleep(60000);
    }
}

bootstrap();