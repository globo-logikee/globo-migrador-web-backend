import { Account, Log, PROCCESS_MESSAGES, PROCCESS_STATUSES } from "../entities";
import { GloboIdApiProvider, SmartInterventionKeys } from "../providers/implementation";
import { LogRepository } from "../repositories/implementation";
import sleep from "../utils/sleep";
import { Any, createConnection, getRepository, Like } from "typeorm";
import format from "date-fns/format";
import { mapSeries } from "p-iteration";

const ProcessStatusRequired = [
    'SALESFORCE_INPUT',
    'CREATED',
    'WAITING_USER_CONFIRM',
]

async function bootstrap() {
    await createConnection().then(() => {
        console.log('Worker not user accept started!');
    }).catch(error => { throw error });

    const accountRepository = getRepository(Account);
    const logRepository = new LogRepository();
    const globoIdApiProvider = new GloboIdApiProvider();

    while (true) {
        const accounts = await accountRepository.find({
            where: {
                $or: [
                    {
                        "processStatus": {
                            $in: ProcessStatusRequired
                        },
                        scheduledTo: format(new Date(), 'yyyy-MM-dd')
                    },
                    {
                        "processStatus": {
                            $in: ProcessStatusRequired
                        },
                        migrationDate: { $lt: new Date() }
                    }
                ]

            }
        })

        if (accounts.length != 0) {
            await mapSeries(accounts, async account => {
                try {
                    const remove = await globoIdApiProvider.removeService(account.globoId, account.serviceId);
                    await globoIdApiProvider.removeService(account.globoId, 84);
                    if (remove.status == 200) {
                        account.processStatus = PROCCESS_STATUSES.WAITING_ACCEPT_LW_OUT;
                        account.status_opt = 4;
                        account.globoIdDeactivationDate = format(new Date(), 'yyyy-MM-dd HH:mm:ss');
                        account.opt = 'out';
                    }

                    await accountRepository.update(account.id, account);

                    const {
                        smart_intervention_globomail_free,
                        smart_intervention_globomail_pro
                    } = SmartInterventionKeys;
                    await globoIdApiProvider.removeSmartIntervention(
                        account.serviceId == 1 ? smart_intervention_globomail_pro : smart_intervention_globomail_free,
                        account.globoId
                    );

                    console.log(`globoId ${account.globoId} on no user accept worker done`);
                } catch (error) {
                    console.log('err worker no user accept', error);

                    const { response } = error;

                    if (response) {
                        if (response.status == 401) {
                            await logRepository.save(new Log({
                                globoId: account.globoId,
                                username: account.username,
                                serviceId: account.serviceId,
                                userIp: account.userIp ? account.userIp : null,
                                message: PROCCESS_MESSAGES.ERR_GLOBOID_API('Unauthorized')
                            }));
                        }
                    }
                }
            });
        }

        await sleep(25000);
    }
}

bootstrap();