import { Account, PROCCESS_STATUSES, Log, PROCCESS_MESSAGES } from "../entities";
import { LocaWebProvider, GloboIdApiProvider, SalesForceProvider } from "../providers/implementation";
import { LogRepository } from "../repositories/implementation";
import sleep from "../utils/sleep";
import { createConnection, getRepository } from "typeorm";
import format from "date-fns/format";
import { mapSeries } from "p-iteration";
import { ParseAccountToSalesForce } from "../utils/parseAccountToSalesforce";

async function bootstrap() {
    await createConnection().then(() => {
        console.log('Worker reactivated users started!');
    }).catch(error => { throw error });

    const accountRepository = getRepository(Account);
    const logRepository = new LogRepository();
    const locaWebProvider = new LocaWebProvider();
    const globoIdApiProvider = new GloboIdApiProvider();
    const salesForceProvicer = new SalesForceProvider();

    while (true) {
        const accounts = await accountRepository.find({
            where: {
                $or: [
                    {
                        processStatus: PROCCESS_STATUSES.REACTIVATED,
                        opt: 'out',
                        reactivatedUntil: format(new Date(), 'yyyy-MM-dd')
                    },
                    {
                        processStatus: PROCCESS_STATUSES.REACTIVATED,
                        opt: 'out',
                        reactivatedUntilDate: { $lt: new Date() }
                    }
                ]
            }
        });

        if (accounts.length != 0) {
            await mapSeries(accounts, async account => {
                try {
                    await globoIdApiProvider.removeService(account.globoId, account.serviceId);
                    await globoIdApiProvider.removeService(account.globoId, 84);
                    const [prefix, domain] = account.email.split('@');
                    await locaWebProvider.updateLocaweb(
                        account.serviceId == 1 ? true : false,
                        prefix,
                        false
                    );

                    account.processStatus = PROCCESS_STATUSES.OPT_OUT_DONE;
                    account.action = 2;

                    await accountRepository.update(account.id, account);

                    await salesForceProvicer.sendToSalesForce(
                        ParseAccountToSalesForce(account)
                    );

                    console.log(`globoId ${account.globoId} on reactivated users worker done`);
                } catch (error) {
                    console.log('err reactivated users worker', error);

                    const { response } = error;

                    if (response) {
                        if (response.status == 404) {
                            const { data } = response;
                            if (data.domain) {
                                await logRepository.save(new Log({
                                    globoId: account.globoId,
                                    username: account.username,
                                    serviceId: account.serviceId,
                                    userIp: account.userIp ? account.userIp : null,
                                    message: PROCCESS_MESSAGES.ERR_UPDATE_LOCAWEB('Not Found', 'domain', data.domain)
                                }));
                            }
                            if (data.mailbox) {
                                await logRepository.save(new Log({
                                    globoId: account.globoId,
                                    username: account.username,
                                    serviceId: account.serviceId,
                                    userIp: account.userIp ? account.userIp : null,
                                    message: PROCCESS_MESSAGES.ERR_UPDATE_LOCAWEB('Not Found', 'mailbox', data.mailbox)
                                }));
                            }
                        }
                    }
                }
            });
        }

        await sleep(25000);
    }
}

bootstrap();