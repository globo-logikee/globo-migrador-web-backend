import { IHasherProvider } from "../../../providers/IHasherProvider";
import { Account, phones } from "../../../entities";
import { ILocaWebCreateModel } from '../../../utils/ILocaWeb';

export class LocaWebInService {
    constructor(
        private hasherProvider: IHasherProvider
    ) { }

    createModelLw(account: Account, decrypted: { password: string, phones: phones, docNumber: string }) {
        const { newEmail, contactEmail, alternativeEmail } = account;

        const emails = [
            {
                address: newEmail,
                main: true,
                confirmed: true
            }
        ];
        if (contactEmail) {
            emails.push({
                address: contactEmail,
                main: false,
                confirmed: true
            });
        }
        if (alternativeEmail) {
            emails.push({
                address: alternativeEmail,
                main: false,
                confirmed: true
            })
        }

        const phones = [];
        if (decrypted.phones) {
            const { dddPhone, phone, dddCellPhone, cellPhone } = decrypted.phones;

            if (phone) {
                phones.push({
                    number: `55-${dddPhone}-${phone}`
                });
            }
            if (cellPhone) {
                phones.push({
                    number: `55-${dddCellPhone}-${cellPhone}`
                });
            }
        }

        const alias_email_address = account.email.split('@')[0] == newEmail.split('@')[0] ?
            null : newEmail.split('@')[0];

        let lwAccount: ILocaWebCreateModel = {
            id_globo: `${account.globoId}/${account.serviceId}`,
            person_type: 'PF',
            current_email_address: account.email,
            alias_email_address,
            password: decrypted.password,
            name: account.fullName,
            cpf: decrypted.docNumber,
            rg: "",
            phones,
            emails,
            address: {
                city: "",
                state: "",
                postal_code: "",
                country: "",
                number: "",
                street: ""
            }
        }

        if (lwAccount.alias_email_address == null) {
            delete lwAccount.alias_email_address;
        }

        return lwAccount;
    }
}