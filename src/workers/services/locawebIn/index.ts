import { LocaWebInService } from './LocawebIn.service';
import { BcryptProvider } from '../../../providers/implementation';

const bcryptProvider = new BcryptProvider();
const locaWebInService = new LocaWebInService(bcryptProvider);

export { locaWebInService };